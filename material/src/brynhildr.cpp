#include				<winsock.h>

#pragma comment(lib,"wsock32")

char					*g_connect_ver;						//通信バージョン
char					*g_ip;								//IP
unsigned short			g_port;								//Port
char					*g_encryption_key;					//パスワード(最大16byte)
SOCKET					g_sock1;							//ソケット(操作系)
SOCKET					g_sock2;							//ソケット(画像系)
SOCKET					g_sock3;							//ソケット(音声系)
long					g_sock1_ct;							//操作系接続カウント
long					g_sock2_ct;							//画像系接続カウント

struct COM_DATA												//ヘッダー(256byte)
{
	char					data_type;						//1:通常データ
	char					thread;							//1:操作系,2:画像系,3:音声系
	char					___filler_1[1];
	char					sound_type;						//0:音声なし,1:音声あり
	long					encryption;						//0:非暗号化通信
	long					data_size;						//データサイズ
	char					___filler_2[4];
	char					check_digit_enc[16];			//チェックデジット(ハッシュ化)
	long					check_digit;					//チェックデジット
	char					ver[4];							//通信バージョン
	long					samplerate;						//サンプルレート
	long					image_cx;						//画像幅
	long					image_cy;						//画像高さ
	long					server_cx;						//サーバー側画面幅
	long					server_cy;						//サーバー側画面高さ
	long					control;						//1:操作
	long					mouse_move;						//0:マウス静止,1:マウス動作
	long					mouse_x;						//マウス座標Ｘ軸
	long					mouse_y;						//マウス座標Ｙ軸
	char					mouse_left;						//1:マウス左ボタンダウン,2:マウス左ボタンアップ
	char					mouse_right;					//1:マウス右ボタンダウン,2:マウス右ボタンアップ
	char					___filler_3[1];
	char					mouse_wheel;					//マウスホイール移動量
	char					keycode;						//キーコード
	char					keycode_flg;					//キーフラグ(0x00:KEYUP,0x80:KEYDOWN)
	char					___filler_4[2];
	char					monitor_no;						//モニター番号
	char					monitor_count;					//モニター数
	char					___filler_5[3];
	long					sound_capture;					//0:DirectSound,1:CoreAudio
	char					___filler_6[44];
	long					video_quality;					//画質(1:最低画質,3:低画質,5:標準画質,7:高画質,9:最高画質)
	char					___filler_7[40];
	long					client_scroll_x;				//スクロール位置Ｘ軸
	long					client_scroll_y;				//スクロール位置Ｙ軸
	char					___filler_8[24];
	double					zoom;							//拡大率(1.0:等倍)
	char					___filler_9[4];
	long					mode;							//5:パブリックモード
	long					sound_quality;					//音質(8000,16000,24000,48000)
	char					___filler_10[20];
};

long send_data(SOCKET *p_sock,char *p_buf,long p_size)
{
	char key[16 + 1];
	memset(&key,'@',16);
	key[16] = 0x00;

	for (long sx1 = 0;sx1 < 16;sx1 ++)
	{
		if (g_encryption_key[sx1] == 0x00)
		{
			break;
		}

		key[sx1] = g_encryption_key[sx1];
	}

	COM_DATA *com_data = (COM_DATA*)p_buf;
	long data_long = com_data->data_type + (com_data->data_size & 0x0000ffff);

	com_data->encryption = 0;
	com_data->check_digit = ~data_long;

	for (long sx1 = 0;sx1 < 16;sx1 ++)
	{
		char key_char = ~key[sx1];
		key_char += (char)(sx1 * ~com_data->check_digit);
		com_data->check_digit_enc[sx1] = key_char;
	}

	return send(*p_sock,p_buf,p_size,0);
}

long recv_data(SOCKET *p_sock,char *p_buf,long p_size)
{
	long size = 0;

	for (;size < p_size;)
	{
		long ret = recv(*p_sock,p_buf + size,p_size - size,0);

		if (ret > 0)
		{
			size += ret;
		}
		else
		{
			size = -1;

			break;
		}
	}

	return size;
}

int WINAPI WinMain(HINSTANCE hInst,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	g_ip = (char *)malloc(32 + 1);
	g_encryption_key = (char *)malloc(16 + 1);
	g_connect_ver = (char *)malloc(4 + 1);

	strcpy(g_ip,"192.168.0.1");								//IP
	g_port = 55500;											//Port

	strcpy(g_connect_ver,"0000");							//通信バージョン
	strcpy(g_encryption_key,"");							//パスワード(最大16byte)

	struct sockaddr_in addr;
	memset(&addr,0,sizeof(sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(g_ip);
	addr.sin_port = htons(g_port);

	WSADATA wsadata;
	WSAStartup(MAKEWORD(1,1),&wsadata);

	g_sock1 = -1;
	g_sock2 = -1;
	g_sock3 = -1;

	g_sock1_ct = 0;
	g_sock2_ct = 0;

	COM_DATA com_data;

	for (;;)
	{
		Sleep(10);

		//---------------------------------------------------------------------------
		//操作系

		if (g_sock1 == -1)
		{
			g_sock1 = socket(AF_INET,SOCK_STREAM,0);

			if (connect(g_sock1,(struct sockaddr *)&addr,sizeof(sockaddr_in)) == SOCKET_ERROR)
			{
				//接続エラー

				break;
			}
		}

		memset(&com_data,0,sizeof(COM_DATA));

		memcpy(&com_data.ver,g_connect_ver,4);

		com_data.data_type = 1;
		com_data.thread = 1;
		com_data.mode = 5;
		com_data.monitor_no = 1;

		//操作系
		com_data.control = 1;
		com_data.mouse_move = 0;
		com_data.mouse_x = 0;
		com_data.mouse_y = 0;
		com_data.mouse_left = 0;
		com_data.mouse_right = 0;
		com_data.mouse_wheel = 0;
		com_data.keycode = 0x00;
		com_data.keycode_flg = 0x00;

		//画像系
		com_data.zoom = 1.0L;
		com_data.image_cx = 1920;
		com_data.image_cy = 1080;
		com_data.client_scroll_x = 0;
		com_data.client_scroll_y = 0;
		com_data.video_quality = 5;

		//音声系
		com_data.sound_type = 1;
		com_data.sound_capture = 1;
		com_data.sound_quality = 24000;

		//ヘッダー送信
		send_data(&g_sock1,(char*)&com_data,sizeof(COM_DATA));

		//ヘッダー受信
		recv_data(&g_sock1,(char*)&com_data,sizeof(COM_DATA));

		if (com_data.mode == 0)
		{
			//パスワードエラー

			break;
		}

		if (g_sock1_ct < 5)
		{
			g_sock1_ct ++;

			continue;
		}

		//---------------------------------------------------------------------------
		//画像系

		if (g_sock2 == -1)
		{
			g_sock2 = socket(AF_INET,SOCK_STREAM,0);

			connect(g_sock2,(struct sockaddr *)&addr,sizeof(sockaddr_in));
		}

		//ヘッダー受信
		recv_data(&g_sock2,(char *)&com_data,sizeof(COM_DATA));

		long image_cx = com_data.image_cx;
		long image_cy = com_data.image_cy;

		long image_size = com_data.data_size;
		char *image_buf = (char *)malloc(image_size);

		//本体受信
		recv_data(&g_sock2,(char *)image_buf,image_size);

		//◆◆◆画像処理を記述◆◆◆

		free(image_buf);

		if (g_sock2_ct < 5)
		{
			g_sock2_ct ++;

			continue;
		}

		//---------------------------------------------------------------------------
		//音声系

		if (g_sock3 == -1)
		{
			g_sock3 = socket(AF_INET,SOCK_STREAM,0);

			connect(g_sock3,(struct sockaddr *)&addr,sizeof(sockaddr_in));
		}

		//ヘッダー受信
		recv_data(&g_sock3,(char *)&com_data,sizeof(COM_DATA));

		long samplerate = com_data.samplerate;

		long sound_size = com_data.data_size;
		char *sound_buf = (char *)malloc(sound_size);

		//本体受信
		recv_data(&g_sock3,(char *)sound_buf,sound_size);

		//◆◆◆音声処理を記述◆◆◆

		free(sound_buf);
	}

	//終了処理

	closesocket(g_sock1);
	closesocket(g_sock2);
	closesocket(g_sock3);

	free(g_ip);
	free(g_encryption_key);
	free(g_connect_ver);

	return 0;
}
