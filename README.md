# CsBrynhildr #


## これは？ ##

IchiGekiさんが開発されているリモート操作ソフト「Brynhildr」を
.NET(C#)とSDLで実装したBrynhildr クライアント クローンです。

## なにがしたかったの？ ##
クライアント側の表示高解像度のが高いときのパフォーマンスを上げられないか、
描画エンジンをSDLにした場合のパフォーマンスを計ってみようと作成しました。

また上記を口実に

* C#でSDLを直接叩いてみよう(P/Invoke)
* Socket通信アプリに挑戦

と言う個人的なプログラムの練習も目的としています。

加えて、自分のほしい機能を実現したBrynhildr クライアント クローンの作成に挑戦しています。
サーバーで解像度を半分にし、クライアントで倍にして描画といった遊びもあります。

※描画速度向上は得られませんでした。

※また、GBVCやGBMJ、Siegfriedの圧縮率の優秀性に打ちのめされる結果になりました。


## 課題 ##
* Thread処理が不安定
* 再接続処理が不安定。ログオン時の再接続は調整中。。。
* 操作遅延がでかい。localhostに接続してマウスを動かしたときの遅延がでかい。
* 入力処理ほぼ未実装。
* 音声再生処理未実装
* Debugメッセージ出力によりパフォーマンスの低下
* 設定ファイルを引数に起動。ファイルを関連付けを行いダブルクリックで接続が目標。
* パスワード未対応
* 暗号化未対応
* Panelのスクロールバーの動きがおかしい

## 注意 ##
* [ブリュンヒルデ零式改。](http://blog.x-row.net/?p=6911) を参考に勝手に開発しています。本家のIchiGekiさんなど、何れの方には関係がありません。
* 本家以上のパフォーマンスを提供できません。
* QtBrynhildr以上のパフォーマンスも提供されません。
* ネットワーク帯域に負荷をかけ圧迫する為、同一ネットワーク内の他の端末が通信できないなどの障害が発生することがあります。本アプリケーションによって起こる如何なる障害・不具合・損害・事故などについて、本家のIchiGekiさんはもちろん、当方も責任を負いかねます。

## 開発環境 ##
* Windows 7/10
* Visual Studio 2013 for Desktop
* C#
* SDL2


## 動作環境 ##
* Intel x86/x64
* Windows 7/8/10
* .NET Framework 4

## Screenshot ##
![ss_v0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.1.png](https://bitbucket.org/libraplanet/csbrynhildr/raw/87bfafff615ad6c9eec1227bbc527a8713350ea4/images/ss_v0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.1.png)

![ss_v0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.2.png](https://bitbucket.org/libraplanet/csbrynhildr/raw/87bfafff615ad6c9eec1227bbc527a8713350ea4/images/ss_v0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.2.png)

## 謝辞 ##
素晴らしい**Brynhildr**を作成されている**IchiGekiさん**を始め、参考にさせて頂きました**QtBrynhildr**を作成されている**ふんふんさん**、その他利用・参考とさせて頂きました、すべてのソフトウェア及びその開発者の方に感謝いたします。

* IchiGekiさん [リモートデスクトップを開発してるエンジニアのブログ。](http://blog.x-row.net/)
* ふんふんさん [ふんふんのブログ](http://mcz-xoxo.cocolog-nifty.com/)
* Qt Brynhildr [funfun-dc5/qtbrynhildr · GitHub](https://github.com/funfun-dc5/qtbrynhildr)
* SDL [Simple DirectMedia Layer](https://www.libsdl.org/)
* SDL2 C# Binder [flibitijibibo/SDL2-CS · GitHub](https://github.com/flibitijibibo/SDL2-CS)
* SDL.NET [C# SDL](http://cs-sdl.sourceforge.net/)

## License ##
MIT License

Copyright (c) 2015 takumi



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:



The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.



THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.