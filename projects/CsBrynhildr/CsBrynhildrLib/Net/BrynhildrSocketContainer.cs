﻿using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace CsBrynhildrLib.Net
{
    public unsafe class BrynhildrSocketkContainer
    {
        public AddressFamily AddressFamily { get; private set; }
        public SocketType SocketType { get; private set; }
        public ProtocolType ProtocolType { get; private set; }

        public string Host { get; private set; }
        public string Port { get; private set; }
        public string Pass { get; private set; }


        public int ConnectedCount { get; private set; }

        private Socket Socket { get; set; }
        private char[] passwordChars;

        public bool Connected
        {
            get
            {
                return Socket.Connected;
            }
        }

        public BrynhildrSocketkContainer(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
        {
            this.AddressFamily = addressFamily;
            this.SocketType = socketType;
            this.ProtocolType = protocolType;
            this.ConnectedCount = 0;
            InitSocket();
            InitPassword(null, null, null);
        }

        public void InitPassword(string host, string port, string pass)
        {
            char[] key = new char[16];
            for (int i = 0; i < key.Length; i++)
            {
                key[i] = '@';
            }
            if (!string.IsNullOrEmpty(pass))
            {
                char[] chars = pass.ToCharArray();
                Array.Copy(chars, key, Math.Min(chars.Length, key.Length));
            }
            this.Host = host;
            this.Port = port;
            this.Pass = pass;
            this.passwordChars = key;
        }

        private void InitSocket()
        {
            Socket = new Socket(AddressFamily, SocketType, ProtocolType);
            Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 10 * 1000);
            Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 10 * 1000);
        }

        public void ResetSocket()
        {
            Close();
            InitSocket();
        }

        public void Connect()
        {
            ConnectedCount = 0;
            //Socket.Connect(Host, int.Parse(Port));
            IAsyncResult result = Socket.BeginConnect(Host, int.Parse(Port), null, null);
            if(!result.AsyncWaitHandle.WaitOne(10 * 1000))
            {
                Socket.Close();
                throw new SocketException((int)SocketError.TimedOut);
            }
        }

        public void Disconnect(bool reuseSocket)
        {
            Socket.Disconnect(reuseSocket);
            ConnectedCount = 0;
        }

        public void Close()
        {
            if (Socket != null)
            {
                try
                {
                    if (Socket.Connected)
                    {
                        Socket.Shutdown(SocketShutdown.Both);
                    }
                }
                catch
                {
                    Socket.Close();
                }
            }
            Socket = null;
            ConnectedCount = 0;
        }

        public int Send(byte[] buffer)
        {
            const int SEG = 1024 * 1024;
            int offset = 0;
            while (offset < buffer.Length)
            {
                int size = Math.Min(buffer.Length - offset, SEG);
                int cnt = Socket.Send(buffer, offset, size, SocketFlags.None);
                offset += cnt;
            }
            return offset;
        }

        public int Receive(byte[] buffer)
        {
            const int SEG = 1024 * 1024;
            int offset = 0;
            while(offset < buffer.Length)
            {
                int size = Math.Min(buffer.Length - offset, SEG);
                int cnt = Socket.Receive(buffer, offset, size, SocketFlags.None);
                offset += cnt;
            }
            return offset;
        }

        public void AddConnectedCount()
        {
            if (ConnectedCount < 1000)
            {
                ConnectedCount++;
            }
        }

        public void SendData(BrynhildrDefine.COM_DATA com_data)
        {
            //check digit
            {
                int data_long = com_data.data_type + (com_data.data_size & 0x0000FFFF);
                com_data.encryption = 0;
                com_data.check_digit = ~data_long;
            }

            //check digit enc
            for (int i = 0; i < passwordChars.Length; i++)
            {
                char key_char = (char)(~passwordChars[i]);
                key_char += (char)(i * ~com_data.check_digit);
                com_data.check_digit_enc[i] = (sbyte)key_char;
            }

            //send
            {
                int len = Marshal.SizeOf(typeof(BrynhildrDefine.COM_DATA));
                byte[] buf = new byte[len];
                unsafe
                {
                    fixed(byte *pBuf = buf)
                    {
                        *(BrynhildrDefine.COM_DATA*)pBuf = com_data;
                    }
                }
                Send(buf);
            }
        }
        public BrynhildrDefine.COM_DATA? ReciveComData()
        {
            int len = Marshal.SizeOf(typeof(BrynhildrDefine.COM_DATA));
            byte[] buf = new byte[len];
            //int readed = Socket.Receive(buf, SocketFlags.None);
            //int readed = stream.Read(buf, 0, buf.Length);
            int readed = Receive(buf);
            if (readed > 0)
            {
                unsafe
                {
                    fixed (byte* p = buf)
                    {
                        BrynhildrDefine.COM_DATA ret = *((BrynhildrDefine.COM_DATA*)p);
                        return ret;
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }
}
