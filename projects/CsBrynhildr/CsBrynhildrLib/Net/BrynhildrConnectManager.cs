﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Runtime.InteropServices;

using CsBrynhildrLib.Commons;
using CsBrynhildrLib.Utils;

namespace CsBrynhildrLib.Net
{
    public class BrynhildrConnectManager
    {
        public string Host { get; private set; }
        public string Port { get; private set; }
        public string Password { get; private set; }

        public Messenger ControlMessenger { get; private set; }
        public Messenger GraphicsMessenger { get; private set; }
        public Messenger SoundMessenger { get; private set; }

        private BrynhildrSocketkContainer socketControl;
        private BrynhildrSocketkContainer socketGraphics;
        private BrynhildrSocketkContainer socketSound;

        private int captureCx = 0;
        private int captureCy = 0;

        private ulong dataSendCount { get; set; }
        private ulong dataRcivCount { get; set; }

        private ulong headerSize = (ulong)Marshal.SizeOf(typeof(BrynhildrDefine.COM_DATA));


        public event Action<int, int> ConnectedSizechange;

        public class ControlInput
        {
            public int MonitorNo { get; set; }
            public double VideoZoom { get; set; }
            public int VideoQuarity { get; set; }
            public sbyte SoundType { get; set; }
            public int SoundCapture { get; set; }
            public int SoundQuarity { get; set; }

            public bool IsControl { get; set; }
            public bool IsMouseMove { get; set; }
            public int MouseX { get; set; }
            public int MouseY { get; set; }
            public sbyte MouseLeft { get; set; }
            public sbyte MouseRight { get; set; }
            public sbyte MouseWheel { get; set; }

            public bool IsKeyDown { get; set; }
            public sbyte KeyCode { get; set; }
            public sbyte KeyFlg { get; set; }

            public ControlInput()
            {
                MonitorNo = 1;
                VideoZoom = 1.0f;
                VideoQuarity = 5;
                SoundType = 1;
                SoundCapture = 1;
                SoundQuarity = 1;
                IsControl = true;
                IsMouseMove = false;
                MouseX = -1;
                MouseY = -1;
                MouseLeft = 0;
                MouseRight = 0;
                MouseWheel = 0;
            }

        }

        public class BrynhildrGraphicsBuffer
        {
            public int Width { get; set; }
            public int Height { get; set; }
            public byte[] Buffer { get; set; }
        }

        public BrynhildrConnectManager(string host, string port, string password)
        {
            AddressFamily addressFamily = AddressFamily.InterNetwork;
            SocketType socketType = SocketType.Stream;
            ProtocolType protocolType = ProtocolType.Unspecified;
            socketControl = new BrynhildrSocketkContainer(addressFamily, socketType, protocolType);
            socketGraphics = new BrynhildrSocketkContainer(addressFamily, socketType, protocolType);
            socketSound = new BrynhildrSocketkContainer(addressFamily, socketType, protocolType);
            socketControl.InitPassword(host, port, password);
            socketGraphics.InitPassword(host, port, password);
            socketSound.InitPassword(host, port, password);
            ControlMessenger = new Messenger();
            GraphicsMessenger = new Messenger();
            SoundMessenger = new Messenger();
        }

        public void ResetSendDataCount()
        {
            lock (this)
            {
                dataSendCount = 0;
            }
        }

        public ulong GetSendDataCount()
        {
            lock (this)
            {
                return dataSendCount;
            }
        }

        public void SetSendDataCount(ulong v)
        {
            lock (this)
            {
                dataSendCount = Math.Min(dataSendCount + v, ulong.MaxValue);
            }
        }

        public void ResetReceiveDataCount()
        {
            lock (this)
            {
                dataRcivCount = 0;
            }
        }

        public ulong GetReceiveDataCount()
        {
            lock (this)
            {
                return dataRcivCount;
            }
        }

        public void SetReceiveDataCount(ulong v)
        {
            lock (this)
            {
                dataRcivCount = Math.Min(dataRcivCount + v, ulong.MaxValue);
            }
        }

        private static void Close(ref BrynhildrSocketkContainer socket)
        {
            try
            {
                if(socket != null)
                {
                    socket.Close();
                }
            }
            finally
            {
                socket = null;
            }
        }

        public void Close()
        {
            Close(ref socketSound);
            Close(ref socketGraphics);
            Close(ref socketControl);
        }

        private void ResetSocket(BrynhildrSocketkContainer socket)
        {
            lock (socket)
            {
                socket.ResetSocket();
            }
        }

        private void DisconnectSocket(BrynhildrSocketkContainer socket)
        {
            lock (socket)
            {
                socket.Disconnect(true);
            }
        }

        public void TickControl(ControlInput controlInput)
        {
            //lock (socketControl)
            {
                BrynhildrSocketkContainer socket = socketControl;
                ControlMessenger.Message("[BrynhildrConnectManager.TickControl()] start. ------------------");
                if (!socket.Connected)
                {
                    try
                    {
                        socket.Connect();
                        ControlMessenger.Message("connect.");
                    }
                    catch(Exception e)
                    {
                        ControlMessenger.Message("appear exception!");
                        ControlMessenger.Message(e);
                        ResetSocket(socketControl);
                        ResetSocket(socketGraphics);
                        ResetSocket(socketSound);
                        ControlMessenger.Message("reset socket.");
                    }
                }

                if (socket.Connected)
                {
                    //送信処理
                    {
                        BrynhildrDefine.COM_DATA com_data = BrynhildrDefine.COM_DATA.CreateNewObject();
                        //version
                        {
                            string ver = "0000";
                            char[] vers = ver.ToCharArray(); ;
                            int max = 4; // Math.Min(ver.Length, Marshal.SizeOf(com_data.ver));
                            unsafe
                            {
                                Marshal.Copy(vers, 0, (IntPtr)com_data.ver, max);
                            }
                        }

                        com_data.data_type = 1;
                        com_data.thread = 1;
                        com_data.mode = 5;
                        com_data.monitor_no = 1;

                        //操作系
                        com_data.control = 1;

                        //画像系
                        com_data.zoom = controlInput.VideoZoom;
                        com_data.image_cx = captureCx;
                        com_data.image_cy = captureCy;
                        com_data.client_scroll_x = 0;
                        com_data.client_scroll_y = 0;
                        com_data.video_quality = controlInput.VideoQuarity;

                        if (controlInput.IsControl)
                        {
                            if (controlInput.IsMouseMove)
                            {
                                com_data.control = 1;
                            }
                            else
                            {
                                com_data.control = 0;
                            }
                            com_data.mouse_x = controlInput.MouseX;
                            com_data.mouse_y = controlInput.MouseY;
                            com_data.mouse_left = controlInput.MouseLeft;
                            com_data.mouse_right = controlInput.MouseRight;
                            com_data.mouse_wheel = controlInput.MouseWheel;
                            com_data.mouse_move = 1;
                        }
                        else
                        {
                            com_data.mouse_move = 0;
                        }

                        if(controlInput.IsKeyDown)
                        {
                            com_data.keycode = controlInput.KeyCode;
                            com_data.keycode_flg = controlInput.KeyFlg;
                            com_data.keydown = 1;
                        }
                        else
                        {
                            com_data.keycode = 0;
                            com_data.keycode_flg = 0;
                            com_data.keydown = 0;
                        }

                        //音声系
                        com_data.sound_type = controlInput.SoundType;
                        com_data.sound_capture = controlInput.SoundCapture;
                        com_data.sound_quality = controlInput.SoundQuarity;

                        //ヘッダ送信
                        socket.SendData(com_data);
                        SetSendDataCount(headerSize);
                    }

                    //受信処理
                    {
                        BrynhildrDefine.COM_DATA? pCom_data = socket.ReciveComData();
                        SetReceiveDataCount(headerSize);

                        if (pCom_data == null)
                        {
                            //受信エラー
                            DisconnectSocket(socketControl);
                            DisconnectSocket(socketGraphics);
                            DisconnectSocket(socketSound);
                        }
                        else
                        {
                            BrynhildrDefine.COM_DATA com_data = pCom_data.Value;
                            if (com_data.mode == 0)
                            {
                                //パスワード エラー
                                DisconnectSocket(socketControl);
                                DisconnectSocket(socketGraphics);
                                DisconnectSocket(socketSound);
                            }
                            else
                            {
                                //受信成功
                                captureCx = com_data.server_cx;
                                captureCy = com_data.server_cy;
                                if (ConnectedSizechange != null)
                                {
                                    ConnectedSizechange(captureCx, captureCy);
                                }
                                socket.AddConnectedCount();
                            }
                        }
                    }
                }
            }
        }

        public BrynhildrGraphicsBuffer TickGraphics()
        {
            //lock (socketControl)
            {
                //lock (socketGraphics)
                {
                    bool connectable = true;
                    connectable &= socketControl.Connected;
                    connectable &= (socketControl.ConnectedCount > 5);

                    BrynhildrSocketkContainer socket = socketGraphics;
                    if (connectable)
                    {
                        GraphicsMessenger.Message("[BrynhildrConnectManager.TickGraphics()] start. ------------------");
                        if (!socket.Connected)
                        {
                            try
                            {
                                socket.Connect();
                                GraphicsMessenger.Message("connect.");
                            }
                            catch (Exception e)
                            {
                                GraphicsMessenger.Message("appear exception!");
                                GraphicsMessenger.Message(e);
                                ResetSocket(socket);
                                GraphicsMessenger.Message("reset socket.");
                            }

                        }
                        if (socket.Connected)
                        {
                            //ヘッダー受信
                            BrynhildrDefine.COM_DATA? pCom_data = socket.ReciveComData();
                            if(pCom_data != null)
                            {
                                SetReceiveDataCount(headerSize);

                                BrynhildrDefine.COM_DATA com_data = pCom_data.Value;
                                int image_cx = com_data.image_cx;
                                int image_cy = com_data.image_cy;
                                int server_cx = com_data.server_cx;
                                int server_cy = com_data.server_cy;
                                int data_size = com_data.data_size;
                                GraphicsMessenger.Message(string.Format("image_cx({0:#,#})", new object[] { image_cx }));
                                GraphicsMessenger.Message(string.Format("image_cy({0:#,#})", new object[] { image_cy }));
                                GraphicsMessenger.Message(string.Format("server_cx({0:#,#})", new object[] { server_cx }));
                                GraphicsMessenger.Message(string.Format("server_cy({0:#,#})", new object[] { server_cy }));
                                GraphicsMessenger.Message(string.Format("data_size({0:#,#})", new object[] { data_size }));
                                //GraphicsMessenger.Message(string.Format("image_cx  ({0})", new object[] { StringUtils.GetBitString(image_cx, 32) }));
                                //GraphicsMessenger.Message(string.Format("image_cy  ({0})", new object[] { StringUtils.GetBitString(image_cy, 32) }));
                                //GraphicsMessenger.Message(string.Format("server_cx ({0})", new object[] { StringUtils.GetBitString(server_cx, 32) }));
                                //GraphicsMessenger.Message(string.Format("server_cy ({0})", new object[] { StringUtils.GetBitString(server_cy, 32) }));
                                //GraphicsMessenger.Message(string.Format("data_size ({0})", new object[] { StringUtils.GetBitString(com_data.data_size, 32) }));
                                //GraphicsMessenger.Message(string.Format("data_size ({0})", new object[] { StringUtils.GetBitString(data_size, 64) }));

                                if (com_data.data_size >= 0)
                                {
                                    byte[] buf = new byte[data_size];
                                    socket.Receive(buf);
                                    socket.AddConnectedCount();
                                    SetReceiveDataCount((ulong)data_size); 
                                    return new BrynhildrGraphicsBuffer() { Width = image_cx, Height = image_cy, Buffer = buf };
                                }
                                else
                                {
                                    //受信失敗
                                    //DisconnectSocket(socketControl);
                                    DisconnectSocket(socketGraphics);
                                    DisconnectSocket(socketSound);
                                    return null;
                                }
                            }
                            else
                            {
                                //受信失敗
                                //DisconnectSocket(socketControl);
                                DisconnectSocket(socketGraphics);
                                DisconnectSocket(socketSound);
                                return null;
                            }
                        }
                        else
                        {
                            //socket.ConnectedCount = 0;
                            return null;
                        }
                    }
                    else
                    {
                        //socket.ConnectedCount = 0;
                        return null;
                    }
                }
            }
        }
        public void TickSound()
        {
            //lock (socketControl)
            {
                //lock (socketSound)
                {
                    bool connectable = true;
                    connectable &= socketGraphics.Connected;
                    connectable &= (socketGraphics.ConnectedCount > 5);
                    BrynhildrSocketkContainer socket = socketSound;
                    if (connectable)
                    {
                        SoundMessenger.Message("[BrynhildrConnectManager.TickSound()] start. ------------------");

                        if (!socket.Connected)
                        {
                            try
                            {
                                socket.Connect();
                                SoundMessenger.Message("connect.");
                            }
                            catch(Exception e)
                            {
                                SoundMessenger.Message("appear exception!");
                                SoundMessenger.Message(e);
                                ResetSocket(socket);
                                SoundMessenger.Message("reset socket.");
                            }
                        }

                        if (socket.Connected)
                        {
                            //ヘッダー受信
                            BrynhildrDefine.COM_DATA? pCcom_data = socket.ReciveComData();
                            if(pCcom_data != null)
                            {
                                SetReceiveDataCount(headerSize);

                                BrynhildrDefine.COM_DATA com_data = pCcom_data.Value;
                                int samplerate = com_data.samplerate;
                                long sound_size = (long)com_data.data_size;
                                SoundMessenger.Message(string.Format("samplerate({0:#,#})", new object[] { samplerate }));
                                SoundMessenger.Message(string.Format("sound_size({0:#,#}L ({1:#,#}))", new object[] { sound_size, com_data.data_size }));
                                if (com_data.data_size >= 0)
                                {
                                    byte[] buf = new byte[sound_size];
                                    socket.Receive(buf);
                                    SetReceiveDataCount((ulong)sound_size);
                                    return;
                                }
                                else
                                {
                                    DisconnectSocket(socketSound);
                                }
                            }
                            else
                            {
                                //受信失敗
                                DisconnectSocket(socketSound);
                            }
                        }
                        else
                        {
                            //socket.ConnectedCount = 0;
                            return;
                        }
                    }
                    else
                    {
                        //socket.ConnectedCount = 0;
                        return;
                    }
                }
            }
        }
    }
}
