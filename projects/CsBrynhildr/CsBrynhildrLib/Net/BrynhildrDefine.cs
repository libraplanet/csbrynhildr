﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace CsBrynhildrLib.Net
{
    public static class BrynhildrDefine
    {
        /// <summary>
        /// //ヘッダー(256byte)
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Size = 256)]
        public unsafe struct COM_DATA
        {
            /// <summary>
            /// 1:通常データ
            /// </summary>
            public sbyte data_type;

            /// <summary>
            /// 1:操作系,2:画像系,3:音声系
            /// </summary>
            public sbyte thread;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_1[1];

            /// <summary>
            /// 0:音声なし,1:音声あり
            /// </summary>
            public sbyte sound_type;

            /// <summary>
            /// 0:非暗号化通信
            /// </summary>
            public int encryption;

            /// <summary>
            /// データサイズ
            /// </summary>
            public int data_size;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_2[4];

            /// <summary>
            /// チェックデジット(ハッシュ化)
            /// </summary>
            public fixed sbyte check_digit_enc[16];

            /// <summary>
            /// チェックデジット
            /// </summary>
            public int check_digit;

            /// <summary>
            /// 通信バージョン
            /// </summary>
            public fixed sbyte ver[4];

            /// <summary>
            /// サンプルレート
            /// </summary>
            public int samplerate;

            /// <summary>
            /// 画像幅
            /// </summary>
            public int image_cx;

            /// <summary>
            /// 画像高さ
            /// </summary>
            public int image_cy;

            /// <summary>
            /// サーバー側画面幅
            /// </summary>
            public int server_cx;

            /// <summary>
            /// サーバー側画面高さ
            /// </summary>
            public int server_cy;

            /// <summary>
            /// 1:操作
            /// </summary>
            public int control;

            /// <summary>
            /// 0:マウス静止,1:マウス動作
            /// </summary>
            public int mouse_move;

            /// <summary>
            /// マウス座標Ｘ軸
            /// </summary>
            public int mouse_x;

            /// <summary>
            /// マウス座標Ｙ軸
            /// </summary>
            public int mouse_y;

            /// <summary>
            /// 1:マウス左ボタンダウン,2:マウス左ボタンアップ,3:マウス左ボタンダブルクリック
            /// </summary>
            public sbyte mouse_left;

            /// <summary>
            /// 1:マウス右ボタンダウン,2:マウス右ボタンアップ,3:マウス右ボタンダブルクリック
            /// </summary>
            public sbyte mouse_right;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_3[1];

            /// <summary>
            /// マウスホイール移動量
            /// </summary>
            public sbyte mouse_wheel;

            /// <summary>
            /// キーコード
            /// </summary>
            public sbyte keycode;

            /// <summary>
            /// キーフラグ(0x00:KEYUP,0x80:KEYDOWN)
            /// </summary>
            public sbyte keycode_flg;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_4[2];

            /// <summary>
            /// モニター番号
            /// </summary>
            public sbyte monitor_no;

            /// <summary>
            /// モニター数
            /// </summary>
            public sbyte monitor_count;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_5[3];

            /// <summary>
            /// 0:DirectSound,1:CoreAudio
            /// </summary>
            public int sound_capture;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_6[40];

            /// <summary>
            /// キー押下(1:押下あり,0:押下なし)
            /// </summary>
            public int keydown;

            /// <summary>
            /// 画質(1:最低画質,3:低画質,5:標準画質,7:高画質,9:最高画質)
            /// </summary>
            public int video_quality;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_7[40];

            /// <summary>
            /// スクロール位置Ｘ軸
            /// </summary>
            public int client_scroll_x;

            /// <summary>
            /// スクロール位置Ｙ軸
            /// </summary>
            public int client_scroll_y;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_8[24];

            /// <summary>
            /// 拡大率(1.0:等倍)
            /// </summary>
            public double zoom;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_9[4];

            /// <summary>
            /// 5:パブリックモード
            /// </summary>
            public int mode;

            /// <summary>
            /// 音質(1:最低音質,2:低音質,3:標準音質,4:高音質,5:最高音質)
            /// </summary>
            public int sound_quality;

            /// <summary>
            /// </summary>
            public fixed sbyte ___filler_10[20];

            public static unsafe COM_DATA CreateNewObject()
            {
                int len = Marshal.SizeOf(typeof(BrynhildrDefine.COM_DATA));
                byte[] buf = new byte[len];
                fixed (byte* p = buf)
                {
                    BrynhildrDefine.COM_DATA ret = *((BrynhildrDefine.COM_DATA*)p);
                    return ret;
                }
            }
        }
    }
}
