﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsBrynhildrLib.Utils
{
    public static class StringUtils
    {
        public static string GetBitString(int n, int x)
        {
            return GetBitString(((long)n) & 0xFFFFFFFF, x);
        }
        public static string GetBitString(long n, int x)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < x; i++)
            {
                int j = x - i - 1;
                int t = (int)((n >> j) & 1);
                sb.Append(t);
                if (((j % 4) == 0) && (j != 0))
                {
                    sb.Append("-");
                }
            }
            return sb.ToString();
        }
    }
}
