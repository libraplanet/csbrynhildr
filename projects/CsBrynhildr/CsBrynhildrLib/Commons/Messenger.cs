﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CsBrynhildrLib.Commons
{
    public class Messenger
    {
        public Action<string> OutputStringMessage;
        public Action<Object> OutputObjectMessage;
        public Action<Exception> OutputExceptionMessage;

        public void Message(string s)
        {
            if (OutputStringMessage != null)
            {
                OutputStringMessage(s);
            }
        }

        public void Message(object o)
        {
            if (OutputObjectMessage != null)
            {
                OutputObjectMessage(o);
            }
        }
        public void Message(Exception e)
        {
            if (OutputExceptionMessage != null)
            {
                OutputExceptionMessage(e);
            }
        }
    }
}
