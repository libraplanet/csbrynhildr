﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;

using CsBrynhildr.Forms;
using CsBrynhildr.Commons;
using CsBrynhildrLib.Net;
using CsBrynhildrLib.Commons;
using Win32;
using SdlWrapper;

namespace CsBrynhildr
{
    public partial class MainForm : Form
    {
        class FullScreenSettingBuffer
        {
            public MainMenu MainMenu {get; set;}
            public FormBorderStyle FormBorderStyle { get; set; }
            public FormWindowState WindowState { get; set; }
        }


        FullScreenSettingBuffer fullScreenSettingBuffer = null;
        BrynhildrSetting currentConnectionSetting = null;

        BrynhildrConnectManager manager = null;

        DebugForm debugForm = new DebugForm();
        Messenger messenger = new Messenger();

        Thread thControl;
        Thread thGraphics;
        Thread thSound;

        Dictionary<MenuItem, int> dictFpsMenu = new Dictionary<MenuItem, int>();
        Dictionary<MenuItem, float> dictRenderScaleMenu = new Dictionary<MenuItem, float>();
        Dictionary<MenuItem, int> dictQualityMenu = new Dictionary<MenuItem, int>();
        Dictionary<MenuItem, float> dictZoomMenu = new Dictionary<MenuItem, float>();
       
        private int sleepTimeMillisec = 1000/60;
        private float renderScale = 1.0f;
        private int videoQuality = 3;
        private float videoZoom = 1.0f;
        private bool isEnableControl = false;

        private ControlInputsRecorder recorder = new ControlInputsRecorder();


        public MainForm()
        {
            //messenger
            InitMessenger(this.messenger, debugForm.AppendGeneralMessage);
            InitLibDir();
            Sdl.SDL_Init(Sdl.SDL_INIT_VIDEO);
            SdlImage.IMG_Init(SdlImage.IMG_INIT_JPG);
            InitializeComponent();
            InitDebugForm();
#if DEBUG
            menuItemTest.Visible = true;
#endif 
            //fps menu map
            {
                dictFpsMenu.Add(this.menuItemFps010, 10);
                dictFpsMenu.Add(this.menuItemFps020, 20);
                dictFpsMenu.Add(this.menuItemFps030, 30);
                dictFpsMenu.Add(this.menuItemFps040, 40);
                dictFpsMenu.Add(this.menuItemFps050, 50);
                dictFpsMenu.Add(this.menuItemFps060, 60);
                dictFpsMenu.Add(this.menuItemFps070, 70);
                dictFpsMenu.Add(this.menuItemFps080, 80);
                dictFpsMenu.Add(this.menuItemFps090, 90);
                dictFpsMenu.Add(this.menuItemFps100, 100);
                dictFpsMenu.Add(this.menuItemFpsMax, 0);
            }
            //window scale menu map
            {
                dictRenderScaleMenu.Add(this.menuItemRenderScale025, 0.25f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale050, 0.50f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale075, 0.75f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale100, 1.00f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale125, 1.25f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale150, 1.50f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale175, 1.75f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale200, 2.00f);
                dictRenderScaleMenu.Add(this.menuItemRenderScale300, 3.00f);
                dictRenderScaleMenu.Add(this.menuItemRenderScaleFit, 0);
            }
            //video quarity menu map
            {
                dictQualityMenu.Add(this.menuItemQualityLowest, 1);
                dictQualityMenu.Add(this.menuItemQualityLow, 3);
                dictQualityMenu.Add(this.menuItemQualityNormal, 5);
                dictQualityMenu.Add(this.menuItemQualityHight, 7);
                dictQualityMenu.Add(this.menuItemQualityHighest, 9);
            }
            //video zoom menu map
            {
                dictZoomMenu.Add(this.menuItemZoom025, 4.0f);
                dictZoomMenu.Add(this.menuItemZoom050, 2.0f);
                dictZoomMenu.Add(this.menuItemZoom075, 1.5f);
                dictZoomMenu.Add(this.menuItemZoom100, 1.0f);
            }
            
            //control
            {
                this.Shown += new EventHandler(CenteringControl);
                this.SizeChanged += new EventHandler(CenteringControl);
                this.sdlControl.SizeChanged += new EventHandler(CenteringControl);
            }
            //sdl
            {
                this.sdlControl.MouseHover += recorder.EventMouseHover;
                this.sdlControl.MouseLeave += recorder.EventMouseLeave;
                this.sdlControl.MouseMove += recorder.EventMouseMove;
                this.sdlControl.MouseDown += recorder.EventMouseDown;
                this.sdlControl.MouseUp += recorder.EventMouseUp;
                this.sdlControl.MouseDoubleClick += recorder.EventMouseDoubleClick;
                this.sdlControl.MouseWheel += recorder.EventMouseWheel;
                this.KeyDown += recorder.EventKeyDown;
                this.KeyUp += recorder.EventKeyUp;
                this.panelRoot.KeyDown += recorder.EventKeyDown;
                this.panelRoot.KeyUp += recorder.EventKeyUp;
                this.sdlControl.KeyDown += recorder.EventKeyDown;
                this.sdlControl.KeyUp += recorder.EventKeyUp;
                recorder.IsFocus = () => { return this.Focused; };
            }

            Sdl.SDL_FillRect(sdlControl.SdlSurface, IntPtr.Zero, 0xFFDD22FF);

            messenger.Message("[MainForm#MainForm()] inited");
        }



        #region [init]

        private void InitDebugForm()
        {
            debugForm.FormClosing += delegate(object sender, FormClosingEventArgs e)
            {
                e.Cancel = true;
                debugForm.Hide();
            };
            debugForm.VisibleChanged += delegate(object sender, EventArgs e)
            {
                menuItemDebugWindow.Checked = debugForm.Visible;
            };
        }

        private void InitMessenger(Messenger m, Action<string> method)
        {
            m.OutputExceptionMessage = delegate(Exception e)
            {
                m.Message(e.ToString());
            };
            m.OutputObjectMessage = delegate(object o)
            {
                if (o == null)
                {
                    m.Message((string)null);
                }
                else
                {
                    m.Message(o.ToString());
                }
            };
            m.OutputStringMessage = delegate(string s)
            {
                method(s);
            };
        }

        private void DisposeMessenger(Messenger m)
        {
            m.OutputExceptionMessage = null;
            m.OutputObjectMessage = null;
            m.OutputStringMessage = null;
        }

        private void InitLibDir()
        {
            string dirPath = Path.GetDirectoryName(Application.ExecutablePath);
            string x86 = Path.Combine(Path.Combine(dirPath, "lib"), "x86");
            string x64 = Path.Combine(Path.Combine(dirPath, "lib"), "x64");
            int bitwidth = IntPtr.Size * 8;
            switch (bitwidth)
            {
                case 32:
                    Api.SetDllDirectory(x86);
                    messenger.Message(string.Format("[MainForm#InitLibDir()] SetDllDirectory({0})", new string[]{x86}));
                   break;
                case 64:
                    Api.SetDllDirectory(x64);
                    messenger.Message(string.Format("[MainForm#InitLibDir()] iniSetDllDirectory({0})", new string[]{x64}));
                    break;
                default:
                    throw new NotSupportedException(string.Format("{0} bit cpu is no support.", new object[] { bitwidth }));

            }
        }

        #endregion

        #region [fps]
        
        private int GetSleepTimeSetting()
        {
            lock (this)
            {
                return sleepTimeMillisec;
            }
        }

        private void SetSleepTimeSetting(int sleepTimeMillisec)
        {
            lock (this)
            {
                this.sleepTimeMillisec = sleepTimeMillisec;
            }
        }

        private void SetFps(int fps)
        {
            if (fps <= 0)
            {
                SetSleepTimeSetting(1);
            }
            else
            {
                SetSleepTimeSetting(1000 / fps);
            }
            foreach (KeyValuePair<MenuItem, int> d in dictFpsMenu)
            {
                d.Key.Checked = (d.Value == fps);
            }
        }

        private void SetRenderScale(float renderScale)
        {
            this.renderScale = Math.Max(0, renderScale);
            foreach (KeyValuePair<MenuItem, float> d in dictRenderScaleMenu)
            {
                d.Key.Checked = (d.Value == renderScale);
            }
        }

        private void SetVideoQuality(int videoQuality)
        {
            if (videoQuality <= 0)
            {
                this.videoQuality = -1;
            }
            else
            {
                this.videoQuality = videoQuality;
            }
            foreach (KeyValuePair<MenuItem, int> d in dictQualityMenu)
            {
                d.Key.Checked = (d.Value == videoQuality);
            }
        }

        private void SetVideoZoom(float videoZoom)
        {
            this.videoZoom = Math.Max(1, videoZoom);
            foreach (KeyValuePair<MenuItem, float> d in dictZoomMenu)
            {
                d.Key.Checked = (d.Value == videoZoom);
            }
        }

        private void SetIsEnableControl(bool isEnableControl)
        {
            this.isEnableControl = isEnableControl;
            this.menuItemInput.Checked = isEnableControl;
        }
        #endregion


        private static void Kill(Thread th)
        {
            try
            {
                th.Abort();
                //for (int i = 0; i < 5; i++ )
                //{
                //    Thread.CurrentThread.Interrupt();
                //    Thread.Yield();
                //    if(!th.IsAlive)
                //    {
                //        break;
                //    }
                //}
            }
            catch
            {

            }

        }

        private void CenteringControl(Control parentControl, Control control)
        {
            int parentWidth = parentControl.ClientSize.Width;
            int parentHeight = parentControl.ClientSize.Height;
            int childWidth = control.Width;
            int childHeight = control.Height;

            if (parentWidth > childWidth)
            {
                control.Left = (parentWidth - childWidth) / 2;
            }
            else
            {
                control.Left = 0;
            }
            if (parentHeight > childHeight)
            {
                control.Top = (parentHeight - childHeight) / 2;
            }
            else
            {
                control.Top = 0;
            }
        }

        public void UpdateStatusMessage(BrynhildrSetting setting)
        {
            string str = "";

            if (setting == null)
            {

            }
            else
            {
                str = string.Format("{0}:{1}", new object[] { setting.Host, setting.Port });
            }

            this.toolStripStatusLabelTraffic.Text = str;
        }

        private void StartBrynhildrControl(BrynhildrSetting setting)
        {
            UpdateStatusMessage(setting);

            this.ClientSize = new Size(setting.ClientX, setting.ClientY);
            this.SetFps(setting.Fps);
            this.SetVideoZoom(setting.VideoZoom);
            this.SetVideoQuality(setting.VideoQuality);
            this.SetRenderScale(setting.RenderScale);
            this.SetIsEnableControl(setting.IsEnableControl);

            manager = new BrynhildrConnectManager(setting.Host, setting.Port, null);

            InitMessenger(manager.ControlMessenger, debugForm.AppendControlMessage);
            InitMessenger(manager.GraphicsMessenger, debugForm.AppendGraphicsMessage);
            InitMessenger(manager.SoundMessenger, debugForm.AppendSoundMessage);

            //control
            {
                //thread
                thControl = new Thread(new ThreadStart(RunThreadControl));
                thGraphics = new Thread(new ThreadStart(RunThreadGraphics));
                thSound = new Thread(new ThreadStart(RunThreadSound));

                thControl.IsBackground = true;
                thGraphics.IsBackground = true;
                thSound.IsBackground = true;

                thControl.Priority = ThreadPriority.BelowNormal;
                thGraphics.Priority = ThreadPriority.Lowest;
                thSound.Priority = ThreadPriority.Lowest;

                thControl.Start();
                thGraphics.Start();
                thSound.Start();
            }
            //timer
            {
                System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
                DateTime lastTime = DateTime.Now;
                timer.Interval = 1000;
                timer.Tick += delegate(object sender, EventArgs ex)
                {
                    if (manager != null)
                    {
                        DateTime time = DateTime.Now;
                        TimeSpan span = time - lastTime;
                        float fps = span.Milliseconds;
                        ulong up = manager.GetSendDataCount();
                        ulong down = manager.GetReceiveDataCount();
                        manager.ResetSendDataCount();
                        manager.ResetReceiveDataCount();
                        double dUp = up * (span.Milliseconds / 1000.0);
                        double dDown = down * (span.Milliseconds / 1000.0);
                        //toolStripStatusLabelTraffic.Text = string.Format("UP:{0:0.00}KB/s, DL:{1:0.00}KB/s", new object[] { dUp / 1024, dDown / 1024 });
                    }
                };
                timer.Start();
            }
        }

        private void UpdateSdlControl(BrynhildrConnectManager.BrynhildrGraphicsBuffer buffer)
        {
            IntPtr pSurface = IntPtr.Zero;
            try
            {
                if (buffer == null)
                {
                    Sdl.SDL_FillRect(sdlControl.SdlSurface, IntPtr.Zero, 0xFF000000);
                    sdlControl.UpdateSdlControl();
                }
                else
                {
                    //control size
                    {
                        Control c = sdlControl;
                        bool update = false;
                        float scale;
                        int width, height;
                        if (renderScale == 0)
                        {
                            float w = (float)panelRoot.Width / buffer.Width;
                            float h = (float)panelRoot.Height / buffer.Height;
                            scale = Math.Min(w, h);
                        }
                        else
                        {
                            scale = renderScale;
                        }
                        width = (int)(buffer.Width * scale);
                        height = (int)(buffer.Height * scale);
                        update |= (c.Width != width);
                        update |= (c.Height != height);
                        if (update)
                        {
                            c.Size = new Size(width, height);
                        }
                    }
                    //surface
                    {
                        long workingSet = Environment.WorkingSet;
                        Sdl.SDL_Rect srcRect, tgtRect;
                        Size size;
                        //rect
                        {
                            srcRect.x = 0;
                            srcRect.y = 0;
                            srcRect.w = buffer.Width;
                            srcRect.h = buffer.Height;
                            tgtRect.x = 0;
                            tgtRect.y = 0;
                            tgtRect.w = sdlControl.Width;
                            tgtRect.h = sdlControl.Height;
                        }
                        //surface
                        {
                            pSurface = SdlUtils.CreateSurface(buffer.Buffer);
                            messenger.Message(string.Format("[UpdateSdlControl()] surface ({0:#,#}) buffer({1:#,#})", new object[] { pSurface.ToString(), buffer.Buffer.Length }));
                        }
                        if (pSurface != IntPtr.Zero)
                        {
                            size = SdlUtils.GetSurfaceSize(pSurface);
                            messenger.Message(string.Format("[UpdateSdlControl()] surface size({0:#,#}, {1:#,#})", new object[] { size.Width, size.Height }));
                            Sdl.SDL_BlitScaled(pSurface, ref srcRect, sdlControl.SdlSurface, ref tgtRect);
                            sdlControl.UpdateSdlControl();
                            {
                                byte[] data = buffer.Buffer;
                                using (MemoryStream stream = new MemoryStream(data))
                                {
                                    using (Image img = Image.FromStream(stream))
                                    {
                                        messenger.Message(string.Format("[UpdateSdlControl()] img size({0:#,#}, {1:#,#})", new object[] { img.Width, img.Height }));
                                    }
                                }
                                data = null;
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                messenger.Message("[UpdateSdlControl()] appear exception!");
                messenger.Message(e);
            }
            finally
            {
                Sdl.SDL_FreeSurface(pSurface);
            }
        }

        #region [menu]

        private void menuItemConnect_Click(object sender, EventArgs e)
        {
            //close
            {
                menuItemDisconnect.PerformClick();
            }
            BrynhildrSetting org = new BrynhildrSetting();
            if (currentConnectionSetting != null)
            {
                org = currentConnectionSetting;
            }
            using (OpenConnectForm f = new OpenConnectForm(org))
            {
                if(f.ShowDialog() == DialogResult.OK)
                {
                    //Open
                    BrynhildrSetting setting = f.BrynhildrSetting;
                    StartBrynhildrControl(setting);
                    currentConnectionSetting = setting;
                }
            }
        }

        private void menuItemOpen_Click(object sender, EventArgs e)
        {
            try
            {
                //close
                {
                    menuItemDisconnect.PerformClick();
                }
                //Open
                {
                    OpenFileDialog dialog = new OpenFileDialog();
                    dialog.Filter = "all file type|*|BrynhildrSetting|*.brynhildr";
                    dialog.FilterIndex = 2;
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        string file = dialog.FileName;
                        BrynhildrSetting setting = BrynhildrSetting.Load(file);
                        StartBrynhildrControl(setting);
                        currentConnectionSetting = setting;
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                messenger.Message(e);
            }
        }

        private void menuItemTest_Click(object sender, EventArgs e)
        {
            try
            {
                //close
                {
                    menuItemDisconnect.PerformClick();
                }
                //close
                {
                    string file = "test.brynhildr";
                    BrynhildrSetting setting = BrynhildrSetting.Load(file);
                    StartBrynhildrControl(setting);
                    currentConnectionSetting = setting;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
                messenger.Message(e);
            }
        }

        private void menuItemDisconnect_Click(object sender, EventArgs e)
        {
            if (manager != null)
            {
                DisposeMessenger(manager.ControlMessenger);
                DisposeMessenger(manager.GraphicsMessenger);
                DisposeMessenger(manager.SoundMessenger);
                manager.Close();
            }

            Kill(thControl);
            Kill(thGraphics);
            Kill(thSound);

            manager = null;
            thControl = null;
            thGraphics = null;
            thSound = null;
            UpdateSdlControl(null);
        }

        private void menuItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void menuItemTopMost_Click(object sender, EventArgs e)
        {
            bool b = !this.TopMost;
            this.TopMost = b;
            this.menuItemTopMost.Checked = b;
        }

        private void menuItemFullScreen_Click(object sender, EventArgs e)
        {
            if (fullScreenSettingBuffer == null)
            {
                fullScreenSettingBuffer = new FullScreenSettingBuffer();
                fullScreenSettingBuffer.FormBorderStyle = this.FormBorderStyle;
                fullScreenSettingBuffer.WindowState = this.WindowState;
                fullScreenSettingBuffer.MainMenu = this.Menu;
                this.FormBorderStyle = FormBorderStyle.None;
                this.WindowState = FormWindowState.Normal;
                this.WindowState = FormWindowState.Maximized;
                this.menuItemFullScreen.Checked = true;
                //this.Menu = null;
                //this.statusStrip1.Visible = false;
            }
            else
            {
                this.FormBorderStyle = fullScreenSettingBuffer.FormBorderStyle;
                this.WindowState = fullScreenSettingBuffer.WindowState;
                this.Menu = fullScreenSettingBuffer.MainMenu;
                fullScreenSettingBuffer = null;
                this.statusStrip.Visible = true;
                this.menuItemFullScreen.Checked = false;
            }
        }

        private void menuItemVersion_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("CsBrynhildr ver {0}", new object[] { "0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.1" }));
            sb.AppendLine(string.Format("  auther : takumi", new object[] { }));
            sb.AppendLine(string.Format("", new object[] { }));
            sb.AppendLine(string.Format("powerd by Brynhildr", new object[] { }));
            MessageBox.Show(sb.ToString(), "version", MessageBoxButtons.OK);
        }

        private void menuItemDebugWindow_Click(object sender, EventArgs e)
        {
            if (debugForm.Visible)
            {
                debugForm.Hide();
            }
            else
            {
                debugForm.Show();
            }
        }

        private void menuItemFps_Click(object sender, EventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            if(dictFpsMenu.ContainsKey(menuItem))
            {
                SetFps(dictFpsMenu[menuItem]);
            }
        }

        private void menuItemRenderScale_Click(object sender, EventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            if (dictRenderScaleMenu.ContainsKey(menuItem))
            {
                SetRenderScale(dictRenderScaleMenu[menuItem]);
            }
        }

        private void menuItemQuality_Click(object sender, EventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            if (dictQualityMenu.ContainsKey(menuItem))
            {
                SetVideoQuality(dictQualityMenu[menuItem]);
            }
        }

        private void menuItemZoom_Click(object sender, EventArgs e)
        {
            MenuItem menuItem = (MenuItem)sender;
            if (dictZoomMenu.ContainsKey(menuItem))
            {
                SetVideoZoom(dictZoomMenu[menuItem]);
            }
        }

        private void menuItemInput_Click(object sender, EventArgs e)
        {
            SetIsEnableControl(!menuItemInput.Checked);
        }

        #endregion

        #region [event(override)]

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
#if DEBUG
            this.menuItemTest.PerformClick();
#endif
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            //close
            menuItemDisconnect.PerformClick();
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
        }

        #endregion

        #region [event (handler)]

        protected void CenteringControl(object sender, EventArgs e)
        {
            CenteringControl(this.panelRoot, this.sdlControl);
        }

        #endregion

        #region [Thread Method]

        private void RunThreadControl()
        {
            try
            {
                BrynhildrConnectManager.ControlInput controlInput = new BrynhildrConnectManager.ControlInput();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                messenger.Message("[MainForm.RunThreadControl()]");
                while (!this.IsDisposed)
                {
                    if (sw.ElapsedMilliseconds > 0)
                    {
                        sw.Restart();
                        try
                        {
                            messenger.Message("[MainForm.RunThreadControl()] loop.");
                            ControlInputsRecorder.Inputs input;
                            int videoQuality = 1;
                            float videoZoom = 1.0f;
                            bool IsControl = false;
                            this.Invoke(new Action(() => {
                                videoQuality = this.videoQuality;
                                videoZoom = this.videoZoom;
                                IsControl = this.isEnableControl;
                            }));
                            input = recorder.GetCurrentInput(true);
                            controlInput.VideoQuarity = videoQuality;
                            controlInput.VideoZoom = videoZoom;
                            controlInput.IsControl = IsControl;
                            controlInput.IsMouseMove = input.IsMouseMove;
                            controlInput.MouseX = (int)(input.MouseX * videoZoom / renderScale);
                            controlInput.MouseY = (int)(input.MouseY * videoZoom / renderScale);
                            controlInput.MouseLeft = (sbyte)input.MouseLeft;
                            controlInput.MouseRight = (sbyte)input.MouseRight;
                            controlInput.MouseWheel = (sbyte)input.MouseWheel;
                            controlInput.IsKeyDown = input.IsKeyDown;
                            controlInput.KeyCode = input.KeyCode;
                            controlInput.KeyFlg = input.KeyFlg;
                            manager.TickControl(controlInput);
                        }
                        catch (ThreadAbortException ex)
                        {
                            throw ex;
                        }
                        catch (Exception ex)
                        {
                            messenger.Message("[MainForm.RunThreadControl()] appear exception!");
                            messenger.Message(ex);
                        }
                    }
                    //Thread.Yield();
                    Thread.Sleep(0);
                }
            }
            catch (ThreadAbortException)
            {
                messenger.Message("[MainForm.RunThreadControl()] abort thread.");
            }
            catch (Exception ex)
            {
                messenger.Message("[MainForm.RunThreadControl()] appear exception!");
                messenger.Message(ex);
            }
            finally
            {
                messenger.Message("[MainForm.RunThreadControl()] close.");
            }
        }

        private void RunThreadGraphics()
        {
            try
            {
                messenger.Message("[MainForm.RunThreadGraphics()] start.");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (!this.IsDisposed)
                {
                    if (sw.ElapsedMilliseconds > GetSleepTimeSetting())
                    {
                        sw.Restart();
                        try
                        {
                            messenger.Message("[MainForm.RunThreadGraphics()] loop.");
                            BrynhildrConnectManager.BrynhildrGraphicsBuffer buffer = manager.TickGraphics();
                            if (buffer != null)
                            {
                                this.Invoke(new Action<BrynhildrConnectManager.BrynhildrGraphicsBuffer>(UpdateSdlControl), new object[] { buffer });
                            }
                        }
                        catch (ThreadAbortException ex)
                        {
                            throw ex;
                        }
                        catch (Exception ex)
                        {
                            messenger.Message("[MainForm.RunThreadGraphics()] appear exception!");
                            messenger.Message(ex);
                        }
                    }
                    //Thread.Yield();
                    Thread.Sleep(0);
                }
            }
            catch (ThreadAbortException)
            {
                messenger.Message("[MainForm.RunThreadGraphics()] abort thread.");
            }
            catch (Exception ex)
            {
                messenger.Message("[MainForm.RunThreadGraphics()] appear exception!");
                messenger.Message(ex);
            }
            finally
            {
                messenger.Message("[MainForm.RunThreadGraphics()] close.");
            }
        }

        private void RunThreadSound()
        {
            try
            {
                messenger.Message("[MainForm.RunThreadSound()] start.");
                Stopwatch sw = new Stopwatch();
                sw.Start();
                while (!this.IsDisposed)
                {
                    if (sw.ElapsedMilliseconds > GetSleepTimeSetting())
                    {
                        sw.Restart();
                        try
                        {
                            messenger.Message("[MainForm.RunThreadSound()] loop.");
                            //soundConnector.Tick();
                            manager.TickSound();
                        }
                        catch (ThreadAbortException ex)
                        {
                            throw ex;
                        }
                        catch (Exception ex)
                        {
                            messenger.Message("[MainForm.RunThreadSound()] appear exception!");
                            messenger.Message(ex);
                        }
                    }
                    //Thread.Yield();
                    Thread.Sleep(0);
                }
            }
            catch (ThreadAbortException)
            {
                messenger.Message("[MainForm.RunThreadSound()] abort thread.");
            }
            catch (Exception ex)
            {
                messenger.Message("[MainForm.RunThreadSound()] appear exception!");
                messenger.Message(ex);
            }
            finally
            {
                messenger.Message("[MainForm.RunThreadSound()] close.");
            }
        }

        #endregion
    }
}
