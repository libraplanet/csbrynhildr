﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class BrynhildrSetting : SerializeBase<BrynhildrSetting>
{
    public string Host { get; set; }
    public string Port { get; set; }
    public int ClientX { get; set; }
    public int ClientY { get; set; }
    public float VideoZoom { get; set; }
    public int VideoQuality { get; set; }
    public float RenderScale { get; set; }
    public int Fps { get; set; }
    public bool IsEnableControl { get; set; }

    public BrynhildrSetting()
    {
        this.Host = "localhost";
        this.Port = "55500";
        this.VideoZoom = 1.0f;
        this.VideoQuality = 5;
        this.Fps = 0;
        this.ClientX = 800;
        this.ClientY = 600;
        this.RenderScale = 1.0f;
        this.IsEnableControl = true;
    }
}
