﻿namespace CsBrynhildr.Forms
{
    partial class DebugForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelRoot = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxSound = new System.Windows.Forms.GroupBox();
            this.textBoxSound = new System.Windows.Forms.TextBox();
            this.groupBoxGraphics = new System.Windows.Forms.GroupBox();
            this.textBoxGraphics = new System.Windows.Forms.TextBox();
            this.groupBoxGeneral = new System.Windows.Forms.GroupBox();
            this.textBoxGeneral = new System.Windows.Forms.TextBox();
            this.groupBoxControl = new System.Windows.Forms.GroupBox();
            this.textBoxControl = new System.Windows.Forms.TextBox();
            this.panel = new System.Windows.Forms.Panel();
            this.checkBoxControl = new System.Windows.Forms.CheckBox();
            this.checkBoxSound = new System.Windows.Forms.CheckBox();
            this.checkBoxGraphics = new System.Windows.Forms.CheckBox();
            this.checkBoxGeneral = new System.Windows.Forms.CheckBox();
            this.button = new System.Windows.Forms.Button();
            this.tableLayoutPanelRoot.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxSound.SuspendLayout();
            this.groupBoxGraphics.SuspendLayout();
            this.groupBoxGeneral.SuspendLayout();
            this.groupBoxControl.SuspendLayout();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelRoot
            // 
            this.tableLayoutPanelRoot.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanelRoot.ColumnCount = 1;
            this.tableLayoutPanelRoot.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRoot.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanelRoot.Controls.Add(this.panel, 0, 0);
            this.tableLayoutPanelRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelRoot.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelRoot.Name = "tableLayoutPanelRoot";
            this.tableLayoutPanelRoot.RowCount = 2;
            this.tableLayoutPanelRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanelRoot.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelRoot.Size = new System.Drawing.Size(897, 611);
            this.tableLayoutPanelRoot.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.groupBoxSound, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxGraphics, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxGeneral, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxControl, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 41);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(889, 566);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // groupBoxSound
            // 
            this.groupBoxSound.Controls.Add(this.textBoxSound);
            this.groupBoxSound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSound.Location = new System.Drawing.Point(447, 286);
            this.groupBoxSound.Name = "groupBoxSound";
            this.groupBoxSound.Size = new System.Drawing.Size(439, 277);
            this.groupBoxSound.TabIndex = 5;
            this.groupBoxSound.TabStop = false;
            this.groupBoxSound.Text = "sound";
            // 
            // textBoxSound
            // 
            this.textBoxSound.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxSound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSound.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxSound.Location = new System.Drawing.Point(3, 15);
            this.textBoxSound.Multiline = true;
            this.textBoxSound.Name = "textBoxSound";
            this.textBoxSound.ReadOnly = true;
            this.textBoxSound.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxSound.Size = new System.Drawing.Size(433, 259);
            this.textBoxSound.TabIndex = 1;
            // 
            // groupBoxGraphics
            // 
            this.groupBoxGraphics.Controls.Add(this.textBoxGraphics);
            this.groupBoxGraphics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxGraphics.Location = new System.Drawing.Point(3, 286);
            this.groupBoxGraphics.Name = "groupBoxGraphics";
            this.groupBoxGraphics.Size = new System.Drawing.Size(438, 277);
            this.groupBoxGraphics.TabIndex = 4;
            this.groupBoxGraphics.TabStop = false;
            this.groupBoxGraphics.Text = "graphics";
            // 
            // textBoxGraphics
            // 
            this.textBoxGraphics.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxGraphics.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxGraphics.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxGraphics.Location = new System.Drawing.Point(3, 15);
            this.textBoxGraphics.Multiline = true;
            this.textBoxGraphics.Name = "textBoxGraphics";
            this.textBoxGraphics.ReadOnly = true;
            this.textBoxGraphics.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxGraphics.Size = new System.Drawing.Size(432, 259);
            this.textBoxGraphics.TabIndex = 1;
            // 
            // groupBoxGeneral
            // 
            this.groupBoxGeneral.Controls.Add(this.textBoxGeneral);
            this.groupBoxGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxGeneral.Location = new System.Drawing.Point(3, 3);
            this.groupBoxGeneral.Name = "groupBoxGeneral";
            this.groupBoxGeneral.Size = new System.Drawing.Size(438, 277);
            this.groupBoxGeneral.TabIndex = 2;
            this.groupBoxGeneral.TabStop = false;
            this.groupBoxGeneral.Text = "general";
            // 
            // textBoxGeneral
            // 
            this.textBoxGeneral.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxGeneral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxGeneral.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxGeneral.Location = new System.Drawing.Point(3, 15);
            this.textBoxGeneral.Multiline = true;
            this.textBoxGeneral.Name = "textBoxGeneral";
            this.textBoxGeneral.ReadOnly = true;
            this.textBoxGeneral.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxGeneral.Size = new System.Drawing.Size(432, 259);
            this.textBoxGeneral.TabIndex = 1;
            // 
            // groupBoxControl
            // 
            this.groupBoxControl.Controls.Add(this.textBoxControl);
            this.groupBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxControl.Location = new System.Drawing.Point(447, 3);
            this.groupBoxControl.Name = "groupBoxControl";
            this.groupBoxControl.Size = new System.Drawing.Size(439, 277);
            this.groupBoxControl.TabIndex = 3;
            this.groupBoxControl.TabStop = false;
            this.groupBoxControl.Text = "control";
            // 
            // textBoxControl
            // 
            this.textBoxControl.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxControl.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBoxControl.Location = new System.Drawing.Point(3, 15);
            this.textBoxControl.Multiline = true;
            this.textBoxControl.Name = "textBoxControl";
            this.textBoxControl.ReadOnly = true;
            this.textBoxControl.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxControl.Size = new System.Drawing.Size(433, 259);
            this.textBoxControl.TabIndex = 1;
            // 
            // panel
            // 
            this.panel.Controls.Add(this.checkBoxControl);
            this.panel.Controls.Add(this.checkBoxSound);
            this.panel.Controls.Add(this.checkBoxGraphics);
            this.panel.Controls.Add(this.checkBoxGeneral);
            this.panel.Controls.Add(this.button);
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(4, 4);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(889, 30);
            this.panel.TabIndex = 0;
            // 
            // checkBoxControl
            // 
            this.checkBoxControl.AutoSize = true;
            this.checkBoxControl.Checked = true;
            this.checkBoxControl.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxControl.Location = new System.Drawing.Point(169, 8);
            this.checkBoxControl.Name = "checkBoxControl";
            this.checkBoxControl.Size = new System.Drawing.Size(59, 16);
            this.checkBoxControl.TabIndex = 2;
            this.checkBoxControl.Text = "control";
            this.checkBoxControl.UseVisualStyleBackColor = true;
            // 
            // checkBoxSound
            // 
            this.checkBoxSound.AutoSize = true;
            this.checkBoxSound.Checked = true;
            this.checkBoxSound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSound.Location = new System.Drawing.Point(309, 8);
            this.checkBoxSound.Name = "checkBoxSound";
            this.checkBoxSound.Size = new System.Drawing.Size(54, 16);
            this.checkBoxSound.TabIndex = 4;
            this.checkBoxSound.Text = "sound";
            this.checkBoxSound.UseVisualStyleBackColor = true;
            // 
            // checkBoxGraphics
            // 
            this.checkBoxGraphics.AutoSize = true;
            this.checkBoxGraphics.Checked = true;
            this.checkBoxGraphics.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGraphics.Location = new System.Drawing.Point(236, 8);
            this.checkBoxGraphics.Name = "checkBoxGraphics";
            this.checkBoxGraphics.Size = new System.Drawing.Size(67, 16);
            this.checkBoxGraphics.TabIndex = 3;
            this.checkBoxGraphics.Text = "graphics";
            this.checkBoxGraphics.UseVisualStyleBackColor = true;
            // 
            // checkBoxGeneral
            // 
            this.checkBoxGeneral.AutoSize = true;
            this.checkBoxGeneral.Checked = true;
            this.checkBoxGeneral.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxGeneral.Location = new System.Drawing.Point(102, 8);
            this.checkBoxGeneral.Name = "checkBoxGeneral";
            this.checkBoxGeneral.Size = new System.Drawing.Size(61, 16);
            this.checkBoxGeneral.TabIndex = 1;
            this.checkBoxGeneral.Text = "general";
            this.checkBoxGeneral.UseVisualStyleBackColor = true;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(6, 4);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(75, 23);
            this.button.TabIndex = 0;
            this.button.Text = "all";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // DebugForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(897, 611);
            this.Controls.Add(this.tableLayoutPanelRoot);
            this.Name = "DebugForm";
            this.Text = "Debug";
            this.tableLayoutPanelRoot.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBoxSound.ResumeLayout(false);
            this.groupBoxSound.PerformLayout();
            this.groupBoxGraphics.ResumeLayout(false);
            this.groupBoxGraphics.PerformLayout();
            this.groupBoxGeneral.ResumeLayout(false);
            this.groupBoxGeneral.PerformLayout();
            this.groupBoxControl.ResumeLayout(false);
            this.groupBoxControl.PerformLayout();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelRoot;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.CheckBox checkBoxGeneral;
        private System.Windows.Forms.CheckBox checkBoxGraphics;
        private System.Windows.Forms.CheckBox checkBoxSound;
        private System.Windows.Forms.CheckBox checkBoxControl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBoxSound;
        private System.Windows.Forms.TextBox textBoxSound;
        private System.Windows.Forms.GroupBox groupBoxGraphics;
        private System.Windows.Forms.TextBox textBoxGraphics;
        private System.Windows.Forms.GroupBox groupBoxGeneral;
        private System.Windows.Forms.TextBox textBoxGeneral;
        private System.Windows.Forms.GroupBox groupBoxControl;
        private System.Windows.Forms.TextBox textBoxControl;
    }
}