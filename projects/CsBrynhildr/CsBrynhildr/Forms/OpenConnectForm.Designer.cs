﻿namespace CsBrynhildr.Forms
{
    partial class OpenConnectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelHost = new System.Windows.Forms.Label();
            this.groupBoxServerSetting = new System.Windows.Forms.GroupBox();
            this.comboBoxPort = new System.Windows.Forms.ComboBox();
            this.comboBoxHost = new System.Windows.Forms.ComboBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxFps = new System.Windows.Forms.ComboBox();
            this.labelFps = new System.Windows.Forms.Label();
            this.comboBoxVideoQuality = new System.Windows.Forms.ComboBox();
            this.comboBoxVideoZoom = new System.Windows.Forms.ComboBox();
            this.labelQuality = new System.Windows.Forms.Label();
            this.labelZoom = new System.Windows.Forms.Label();
            this.groupBoxWindow = new System.Windows.Forms.GroupBox();
            this.comboBoxRenderScale = new System.Windows.Forms.ComboBox();
            this.labelWindowRenderScale = new System.Windows.Forms.Label();
            this.comboBoxClientHeight = new System.Windows.Forms.ComboBox();
            this.comboBoxClientWidth = new System.Windows.Forms.ComboBox();
            this.labelClientHeight = new System.Windows.Forms.Label();
            this.labelClientWidth = new System.Windows.Forms.Label();
            this.groupBoxOther = new System.Windows.Forms.GroupBox();
            this.checkBoxEnableInput = new System.Windows.Forms.CheckBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBoxServerSetting.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBoxWindow.SuspendLayout();
            this.groupBoxOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // labelHost
            // 
            this.labelHost.AutoSize = true;
            this.labelHost.Location = new System.Drawing.Point(6, 27);
            this.labelHost.Name = "labelHost";
            this.labelHost.Size = new System.Drawing.Size(29, 12);
            this.labelHost.TabIndex = 0;
            this.labelHost.Text = "Host";
            // 
            // groupBoxServerSetting
            // 
            this.groupBoxServerSetting.Controls.Add(this.comboBoxPort);
            this.groupBoxServerSetting.Controls.Add(this.comboBoxHost);
            this.groupBoxServerSetting.Controls.Add(this.labelPort);
            this.groupBoxServerSetting.Controls.Add(this.labelHost);
            this.groupBoxServerSetting.Location = new System.Drawing.Point(12, 12);
            this.groupBoxServerSetting.Name = "groupBoxServerSetting";
            this.groupBoxServerSetting.Size = new System.Drawing.Size(217, 110);
            this.groupBoxServerSetting.TabIndex = 0;
            this.groupBoxServerSetting.TabStop = false;
            this.groupBoxServerSetting.Text = "Server";
            // 
            // comboBoxPort
            // 
            this.comboBoxPort.FormattingEnabled = true;
            this.comboBoxPort.Location = new System.Drawing.Point(72, 49);
            this.comboBoxPort.Name = "comboBoxPort";
            this.comboBoxPort.Size = new System.Drawing.Size(110, 20);
            this.comboBoxPort.TabIndex = 3;
            // 
            // comboBoxHost
            // 
            this.comboBoxHost.FormattingEnabled = true;
            this.comboBoxHost.Location = new System.Drawing.Point(72, 24);
            this.comboBoxHost.Name = "comboBoxHost";
            this.comboBoxHost.Size = new System.Drawing.Size(110, 20);
            this.comboBoxHost.TabIndex = 1;
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(6, 52);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(26, 12);
            this.labelPort.TabIndex = 2;
            this.labelPort.Text = "Port";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxFps);
            this.groupBox1.Controls.Add(this.labelFps);
            this.groupBox1.Controls.Add(this.comboBoxVideoQuality);
            this.groupBox1.Controls.Add(this.comboBoxVideoZoom);
            this.groupBox1.Controls.Add(this.labelQuality);
            this.groupBox1.Controls.Add(this.labelZoom);
            this.groupBox1.Location = new System.Drawing.Point(235, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(202, 110);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Video";
            // 
            // comboBoxFps
            // 
            this.comboBoxFps.FormattingEnabled = true;
            this.comboBoxFps.Location = new System.Drawing.Point(62, 75);
            this.comboBoxFps.Name = "comboBoxFps";
            this.comboBoxFps.Size = new System.Drawing.Size(104, 20);
            this.comboBoxFps.TabIndex = 5;
            // 
            // labelFps
            // 
            this.labelFps.AutoSize = true;
            this.labelFps.Location = new System.Drawing.Point(6, 78);
            this.labelFps.Name = "labelFps";
            this.labelFps.Size = new System.Drawing.Size(26, 12);
            this.labelFps.TabIndex = 4;
            this.labelFps.Text = "FPS";
            // 
            // comboBoxVideoQuality
            // 
            this.comboBoxVideoQuality.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVideoQuality.FormattingEnabled = true;
            this.comboBoxVideoQuality.Location = new System.Drawing.Point(62, 49);
            this.comboBoxVideoQuality.Name = "comboBoxVideoQuality";
            this.comboBoxVideoQuality.Size = new System.Drawing.Size(104, 20);
            this.comboBoxVideoQuality.TabIndex = 3;
            // 
            // comboBoxVideoZoom
            // 
            this.comboBoxVideoZoom.FormattingEnabled = true;
            this.comboBoxVideoZoom.Location = new System.Drawing.Point(62, 24);
            this.comboBoxVideoZoom.Name = "comboBoxVideoZoom";
            this.comboBoxVideoZoom.Size = new System.Drawing.Size(104, 20);
            this.comboBoxVideoZoom.TabIndex = 1;
            // 
            // labelQuality
            // 
            this.labelQuality.AutoSize = true;
            this.labelQuality.Location = new System.Drawing.Point(6, 52);
            this.labelQuality.Name = "labelQuality";
            this.labelQuality.Size = new System.Drawing.Size(41, 12);
            this.labelQuality.TabIndex = 2;
            this.labelQuality.Text = "Quality";
            // 
            // labelZoom
            // 
            this.labelZoom.AutoSize = true;
            this.labelZoom.Location = new System.Drawing.Point(6, 27);
            this.labelZoom.Name = "labelZoom";
            this.labelZoom.Size = new System.Drawing.Size(33, 12);
            this.labelZoom.TabIndex = 0;
            this.labelZoom.Text = "Zoom";
            // 
            // groupBoxWindow
            // 
            this.groupBoxWindow.Controls.Add(this.comboBoxRenderScale);
            this.groupBoxWindow.Controls.Add(this.labelWindowRenderScale);
            this.groupBoxWindow.Controls.Add(this.comboBoxClientHeight);
            this.groupBoxWindow.Controls.Add(this.comboBoxClientWidth);
            this.groupBoxWindow.Controls.Add(this.labelClientHeight);
            this.groupBoxWindow.Controls.Add(this.labelClientWidth);
            this.groupBoxWindow.Location = new System.Drawing.Point(12, 128);
            this.groupBoxWindow.Name = "groupBoxWindow";
            this.groupBoxWindow.Size = new System.Drawing.Size(217, 106);
            this.groupBoxWindow.TabIndex = 2;
            this.groupBoxWindow.TabStop = false;
            this.groupBoxWindow.Text = "Window";
            // 
            // comboBoxRenderScale
            // 
            this.comboBoxRenderScale.FormattingEnabled = true;
            this.comboBoxRenderScale.Location = new System.Drawing.Point(88, 75);
            this.comboBoxRenderScale.Name = "comboBoxRenderScale";
            this.comboBoxRenderScale.Size = new System.Drawing.Size(94, 20);
            this.comboBoxRenderScale.TabIndex = 5;
            // 
            // labelWindowRenderScale
            // 
            this.labelWindowRenderScale.AutoSize = true;
            this.labelWindowRenderScale.Location = new System.Drawing.Point(6, 78);
            this.labelWindowRenderScale.Name = "labelWindowRenderScale";
            this.labelWindowRenderScale.Size = new System.Drawing.Size(73, 12);
            this.labelWindowRenderScale.TabIndex = 4;
            this.labelWindowRenderScale.Text = "Render Scale";
            // 
            // comboBoxClientHeight
            // 
            this.comboBoxClientHeight.FormattingEnabled = true;
            this.comboBoxClientHeight.Location = new System.Drawing.Point(88, 49);
            this.comboBoxClientHeight.Name = "comboBoxClientHeight";
            this.comboBoxClientHeight.Size = new System.Drawing.Size(94, 20);
            this.comboBoxClientHeight.TabIndex = 3;
            // 
            // comboBoxClientWidth
            // 
            this.comboBoxClientWidth.FormattingEnabled = true;
            this.comboBoxClientWidth.Location = new System.Drawing.Point(88, 24);
            this.comboBoxClientWidth.Name = "comboBoxClientWidth";
            this.comboBoxClientWidth.Size = new System.Drawing.Size(94, 20);
            this.comboBoxClientWidth.TabIndex = 1;
            // 
            // labelClientHeight
            // 
            this.labelClientHeight.AutoSize = true;
            this.labelClientHeight.Location = new System.Drawing.Point(6, 52);
            this.labelClientHeight.Name = "labelClientHeight";
            this.labelClientHeight.Size = new System.Drawing.Size(72, 12);
            this.labelClientHeight.TabIndex = 2;
            this.labelClientHeight.Text = "Client Height";
            // 
            // labelClientWidth
            // 
            this.labelClientWidth.AutoSize = true;
            this.labelClientWidth.Location = new System.Drawing.Point(6, 27);
            this.labelClientWidth.Name = "labelClientWidth";
            this.labelClientWidth.Size = new System.Drawing.Size(67, 12);
            this.labelClientWidth.TabIndex = 0;
            this.labelClientWidth.Text = "Client Width";
            // 
            // groupBoxOther
            // 
            this.groupBoxOther.Controls.Add(this.checkBoxEnableInput);
            this.groupBoxOther.Location = new System.Drawing.Point(235, 128);
            this.groupBoxOther.Name = "groupBoxOther";
            this.groupBoxOther.Size = new System.Drawing.Size(202, 106);
            this.groupBoxOther.TabIndex = 3;
            this.groupBoxOther.TabStop = false;
            this.groupBoxOther.Text = "Other";
            // 
            // checkBoxEnableInput
            // 
            this.checkBoxEnableInput.AutoSize = true;
            this.checkBoxEnableInput.Location = new System.Drawing.Point(8, 19);
            this.checkBoxEnableInput.Name = "checkBoxEnableInput";
            this.checkBoxEnableInput.Size = new System.Drawing.Size(87, 16);
            this.checkBoxEnableInput.TabIndex = 0;
            this.checkBoxEnableInput.Text = "Enable Input";
            this.checkBoxEnableInput.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(197, 240);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 4;
            this.buttonSave.Text = "&Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(278, 240);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 5;
            this.buttonOk.Text = "&OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(359, 240);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 6;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            // 
            // OpenConnectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 274);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.groupBoxOther);
            this.Controls.Add(this.groupBoxWindow);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxServerSetting);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "OpenConnectForm";
            this.Text = "Connection";
            this.groupBoxServerSetting.ResumeLayout(false);
            this.groupBoxServerSetting.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBoxWindow.ResumeLayout(false);
            this.groupBoxWindow.PerformLayout();
            this.groupBoxOther.ResumeLayout(false);
            this.groupBoxOther.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelHost;
        private System.Windows.Forms.GroupBox groupBoxServerSetting;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelQuality;
        private System.Windows.Forms.Label labelZoom;
        private System.Windows.Forms.ComboBox comboBoxVideoQuality;
        private System.Windows.Forms.ComboBox comboBoxVideoZoom;
        private System.Windows.Forms.GroupBox groupBoxWindow;
        private System.Windows.Forms.ComboBox comboBoxClientHeight;
        private System.Windows.Forms.ComboBox comboBoxClientWidth;
        private System.Windows.Forms.Label labelClientHeight;
        private System.Windows.Forms.Label labelClientWidth;
        private System.Windows.Forms.ComboBox comboBoxRenderScale;
        private System.Windows.Forms.Label labelWindowRenderScale;
        private System.Windows.Forms.ComboBox comboBoxFps;
        private System.Windows.Forms.Label labelFps;
        private System.Windows.Forms.GroupBox groupBoxOther;
        private System.Windows.Forms.CheckBox checkBoxEnableInput;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ErrorProvider errorProvider;
        private System.Windows.Forms.ComboBox comboBoxPort;
        private System.Windows.Forms.ComboBox comboBoxHost;
    }
}