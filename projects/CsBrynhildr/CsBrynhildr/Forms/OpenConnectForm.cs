﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CsBrynhildr.Forms
{
    public partial class OpenConnectForm : Form
    {
        class PairBase<T>
        {
            public string Caption { get; protected set; }
            public T Value { get; protected set; }

            public PairBase(string caption, T value)
            {
                this.Caption = caption;
                this.Value = value;
            }

            public override string ToString()
            {
                return Caption;
            }
        }

        class IntPair : PairBase<int>
        {
            public IntPair(string caption, int value)
                : base(caption, value)
            {
            }
       }
        class FloatPair : PairBase<float>
        {
            public FloatPair(string caption, float value)
                : base(caption, value)
            {
            }
        }
        class StringPair : PairBase<string>
        {
            public StringPair(string caption, string value)
                : base(caption, value)
            {
            }
       }
        static List<StringPair> SERVER_HOST_LIST = new List<StringPair>();
        static List<StringPair> SERVER_PORT_LIST = new List<StringPair>();
        static List<FloatPair> VIDEO_ZOOM_LIST = new List<FloatPair>();
        static List<IntPair> VIDEO_QUALITY_LIST = new List<IntPair>();
        static List<IntPair> VIDEO_FPS_LIST = new List<IntPair>();
        static List<IntPair> WINDOW_CLIENT_WIDTH = new List<IntPair>();
        static List<IntPair> WINDOW_CLIENT_HEIGHT = new List<IntPair>();
        static List<FloatPair> WINDOW_RENDER_SCALE_LIST = new List<FloatPair>();
        
        public BrynhildrSetting BrynhildrSetting { get; private set; }
        
        static OpenConnectForm()
        {
            //this.comboBoxVideoZoom.Items.a
            //host list
            {
                SERVER_HOST_LIST.Add(new StringPair("127.0.0.1", "127.0.0.1"));
                SERVER_HOST_LIST.Add(new StringPair("localhost", "localhost"));
            }
            //port list
            {
                SERVER_PORT_LIST.Add(new StringPair("[siegfried] 49900", "49900"));
                SERVER_PORT_LIST.Add(new StringPair("[brynhildr] 55500", "55500"));
            }
            //fps list
            {
                VIDEO_FPS_LIST.Add(new IntPair("10", 10));
                VIDEO_FPS_LIST.Add(new IntPair("20", 20));
                VIDEO_FPS_LIST.Add(new IntPair("30", 30));
                VIDEO_FPS_LIST.Add(new IntPair("40", 40));
                VIDEO_FPS_LIST.Add(new IntPair("50", 50));
                VIDEO_FPS_LIST.Add(new IntPair("60", 60));
                VIDEO_FPS_LIST.Add(new IntPair("70", 70));
                VIDEO_FPS_LIST.Add(new IntPair("80", 80));
                VIDEO_FPS_LIST.Add(new IntPair("90", 90));
                VIDEO_FPS_LIST.Add(new IntPair("100", 100));
                VIDEO_FPS_LIST.Add(new IntPair("MAX", 0));
            }
            //video quarity list
            {
                VIDEO_QUALITY_LIST.Add(new IntPair("Lowest", 1));
                VIDEO_QUALITY_LIST.Add(new IntPair("Low", 3));
                VIDEO_QUALITY_LIST.Add(new IntPair("Normal", 5));
                VIDEO_QUALITY_LIST.Add(new IntPair("Hight", 7));
                VIDEO_QUALITY_LIST.Add(new IntPair("Highest", 9));
            }
            //video zoom list
            {
                VIDEO_ZOOM_LIST.Add(new FloatPair("25%", 4.0f));
                VIDEO_ZOOM_LIST.Add(new FloatPair("50%", 2.0f));
                VIDEO_ZOOM_LIST.Add(new FloatPair("75%", 1.5f));
                VIDEO_ZOOM_LIST.Add(new FloatPair("100%", 1.0f));
            }
            //window client width list
            {
                WINDOW_CLIENT_WIDTH.Add(new IntPair("640px", 640));
                WINDOW_CLIENT_WIDTH.Add(new IntPair("800px", 800));
                WINDOW_CLIENT_WIDTH.Add(new IntPair("1024px", 1024));
                WINDOW_CLIENT_WIDTH.Add(new IntPair("1152px", 1152));
                WINDOW_CLIENT_WIDTH.Add(new IntPair("1280px", 1280));
                
            }
            //window client height list
            {
                WINDOW_CLIENT_HEIGHT.Add(new IntPair("480px", 480));
                WINDOW_CLIENT_HEIGHT.Add(new IntPair("600px", 600));
                WINDOW_CLIENT_HEIGHT.Add(new IntPair("720px", 720));
                WINDOW_CLIENT_HEIGHT.Add(new IntPair("768px", 768));
                WINDOW_CLIENT_HEIGHT.Add(new IntPair("864px", 864));
            }
            //video render menu map list
            {
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("25%", 0.25f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("50%", 0.50f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("75%", 0.75f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("100%", 1.00f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("125%", 1.25f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("150%", 1.50f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("175%", 1.75f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("200%", 2.00f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("300%", 3.00f));
                WINDOW_RENDER_SCALE_LIST.Add(new FloatPair("FIT", 0));
            }
        }
        
        public OpenConnectForm(BrynhildrSetting org)
        {
            InitializeComponent();
            SuspendLayout();
            try
            {
                BrynhildrSetting = org.Clone();
                this.comboBoxHost.Items.AddRange(SERVER_HOST_LIST.ToArray());
                this.comboBoxPort.Items.AddRange(SERVER_PORT_LIST.ToArray());
                this.comboBoxVideoZoom.Items.AddRange(VIDEO_ZOOM_LIST.ToArray());
                this.comboBoxVideoQuality.Items.AddRange(VIDEO_QUALITY_LIST.ToArray());
                this.comboBoxFps.Items.AddRange(VIDEO_FPS_LIST.ToArray());
                this.comboBoxClientWidth.Items.AddRange(WINDOW_CLIENT_WIDTH.ToArray());
                this.comboBoxClientHeight.Items.AddRange(WINDOW_CLIENT_HEIGHT.ToArray());
                this.comboBoxRenderScale.Items.AddRange(WINDOW_RENDER_SCALE_LIST.ToArray());

                SetComboValue(this.comboBoxHost, "localhost");
                SetComboValue(this.comboBoxPort, "55500");
                SetComboValue(this.comboBoxVideoZoom, 1.0f);
                SetComboValue(this.comboBoxVideoQuality, 5);
                SetComboValue(this.comboBoxFps, 0);
                SetComboValue(this.comboBoxClientWidth, 800);
                SetComboValue(this.comboBoxClientHeight, 600);
                SetComboValue(this.comboBoxRenderScale, 1.0f);

                SetComboValue(this.comboBoxHost, BrynhildrSetting.Host);
                SetComboValue(this.comboBoxPort, BrynhildrSetting.Port);
                SetComboValue(this.comboBoxVideoZoom, BrynhildrSetting.VideoZoom);
                SetComboValue(this.comboBoxVideoQuality, BrynhildrSetting.VideoQuality);
                SetComboValue(this.comboBoxFps, BrynhildrSetting.Fps);
                SetComboValue(this.comboBoxClientWidth, BrynhildrSetting.ClientX);
                SetComboValue(this.comboBoxClientHeight, BrynhildrSetting.ClientY);
                SetComboValue(this.comboBoxRenderScale, BrynhildrSetting.RenderScale);
                this.checkBoxEnableInput.Checked = BrynhildrSetting.IsEnableControl;

                this.comboBoxHost.Validating += this.comboBoxVideo_Validating;
                this.comboBoxPort.Validating += this.comboBoxVideo_Validating;
                this.comboBoxVideoZoom.Validating += this.comboBoxVideo_Validating;
                this.comboBoxVideoQuality.Validating += this.comboBoxVideo_Validating;
                this.comboBoxFps.Validating += this.comboBoxVideo_Validating;
                this.comboBoxClientWidth.Validating += this.comboBoxVideo_Validating;
                this.comboBoxClientHeight.Validating += this.comboBoxVideo_Validating;
                this.comboBoxRenderScale.Validating += this.comboBoxVideo_Validating;
            }
            finally
            {
                ResumeLayout(false);
            }
        }

        int GetComboValueInt(ComboBox c)
        {
            object obj = c.SelectedItem;
            if (obj == null)
            {
                return int.Parse((string)c.Text);
            }
            else
            {
                return ((IntPair)obj).Value;
            }
        }

        float GetComboValueFloat(ComboBox c)
        {
            object obj = c.SelectedItem;
            if (obj == null)
            {
                return int.Parse((string)c.Text);
            }
            else
            {
                return ((FloatPair)obj).Value;
            }
        }

        string GetComboValueString(ComboBox c)
        {
            object obj = c.SelectedItem;
            if (obj == null)
            {
                return ((string)c.Text);
            }
            else
            {
                return ((StringPair)obj).Value;
            }
        }

        void SetComboValue(ComboBox c, object value)
        {
            foreach(object obj in c.Items)
            {
                if ((obj is IntPair) && ((IntPair)obj).Value.Equals(value))
                {
                    c.SelectedItem = obj;
                    return;
                }
                else if ((obj is FloatPair) && ((FloatPair)obj).Value.Equals(value))
                {
                    c.SelectedItem = obj;
                    return;
                }
                else if ((obj is StringPair) && ((StringPair)obj).Value.Equals(value))
                {
                    c.SelectedItem = obj;
                    return;
                }
            }
            if (value == null)
            {
                c.SelectedItem = null;
                c.Text = null;
            }
            else
            {
                c.SelectedItem = null;
                c.Text = value.ToString();
            }
        }

        BrynhildrSetting CreateSetting()
        {
            BrynhildrSetting setting = new BrynhildrSetting();
            setting.Host = GetComboValueString(this.comboBoxHost);
            setting.Port = "" + GetComboValueString(this.comboBoxPort);
            setting.VideoZoom = GetComboValueFloat(this.comboBoxVideoZoom);
            setting.VideoQuality = GetComboValueInt(this.comboBoxVideoQuality);
            setting.Fps = GetComboValueInt(this.comboBoxFps);
            setting.ClientX = GetComboValueInt(this.comboBoxClientWidth);
            setting.ClientY = GetComboValueInt(this.comboBoxClientHeight);
            setting.RenderScale = GetComboValueFloat(this.comboBoxRenderScale);
            setting.IsEnableControl = this.checkBoxEnableInput.Checked;
            return setting;
        }

        #region [button]
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if(ValidateAll())
            {
                BrynhildrSetting setting = CreateSetting();
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.Filter = "all file type|*|BrynhildrSetting|*.brynhildr";
                dialog.FilterIndex = 2;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    setting.Save(dialog.FileName);
                }
            }
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if(ValidateAll())
            {
                BrynhildrSetting setting = CreateSetting();
                BrynhildrSetting = setting;
                this.DialogResult = DialogResult.OK;
            }

        }
        #endregion

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            BrynhildrSetting = null;
        }

        private bool ValidateAll()
        {
            bool ret = true;
            ret &= Validate(this.comboBoxHost);
            ret &= Validate(this.comboBoxPort);
            ret &= Validate(this.comboBoxVideoZoom);
            ret &= Validate(this.comboBoxVideoQuality);
            ret &= Validate(this.comboBoxFps);
            ret &= Validate(this.comboBoxClientWidth);
            ret &= Validate(this.comboBoxClientHeight);
            ret &= Validate(this.comboBoxRenderScale);
            return ret;
        }

        private bool Validate(Control control)
        {
            if (control == null)
            {
                return false;
            }
            else
            {
                Func<string, Action, bool> validate = (string errorMessage, Action action) =>
                {
                    try
                    {
                        action();
                        errorProvider.SetError(control, "");
                        return true;
                    }
                    catch
                    {
                        errorProvider.SetError(control, "");
                        errorProvider.SetError(control, errorMessage);
                        //e.Cancel = true;
                        return false;
                    }
                };

                if (control == this.comboBoxHost)
                {
                    return validate("IPかホスト名を入力してください。", () =>
                    {
                        string host = GetComboValueString(this.comboBoxHost);
                        if (string.IsNullOrEmpty(host))
                        {
                            throw new Exception();
                        }
                    });
                }
                else if (control == this.comboBoxPort)
                {
                    return validate("1～65535で入力してください。", () =>
                    {
                        string strPort = GetComboValueString(this.comboBoxPort);
                        int port = int.Parse(strPort);
                        if (port < 0)
                        {
                            throw new Exception();
                        }
                        if (port > 65535)
                        {
                            throw new Exception();
                        }
                    });
                }
                else if (control == this.comboBoxVideoZoom)
                {
                    return validate("小数で入力してください。", () =>
                    {
                        float zoom = GetComboValueFloat(this.comboBoxVideoZoom);
                        if (zoom < 0)
                        {
                            throw new Exception();
                        }
                        if (zoom > 1000)
                        {
                            throw new Exception();
                        }
                    });
                }
                else if (control == this.comboBoxVideoQuality)
                {
                    return validate("1～9で入力してください。", () =>
                    {
                        int quality = GetComboValueInt(this.comboBoxVideoQuality);
                        if ((quality < 1) || (quality > 9))
                        {
                            throw new Exception();
                        }
                    });
                }
                else if (control == this.comboBoxFps)
                {
                    return validate("自然数で入力してください。", () =>
                    {
                        if (!string.Equals(this.comboBoxFps.Text, "MAX"))
                        {
                            int fps = GetComboValueInt(this.comboBoxFps);
                            if (fps < 0)
                            {
                                throw new Exception();
                            }
                        }
                    });
                }
                else if (control == this.comboBoxClientWidth)
                {
                    return validate("自然数で入力してください。", () =>
                    {
                        int clientWidth = GetComboValueInt(this.comboBoxClientWidth);
                        if (clientWidth < 0)
                        {
                            throw new Exception();
                        }
                    });
                }
                else if (control == this.comboBoxClientHeight)
                {
                    return validate("自然数で入力してください。", () =>
                    {
                        int clientHeight = GetComboValueInt(this.comboBoxClientHeight);
                        if (clientHeight < 0)
                        {
                            throw new Exception();
                        }
                    });
                }
                else if (control == this.comboBoxRenderScale)
                {
                    return validate("小数で入力してください。", () =>
                    {
                        float windowScale = GetComboValueFloat(this.comboBoxRenderScale);
                        if (windowScale < 0)
                        {
                            throw new Exception();
                        }
                    });
                }
                else
                {
                    return false;
                }
            }
        }

        private void comboBoxVideo_Validating(object sender, CancelEventArgs e)
        {
            Validate(sender as Control);
        }
    }
}
