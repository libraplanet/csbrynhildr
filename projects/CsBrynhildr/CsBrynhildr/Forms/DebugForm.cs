﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CsBrynhildr.Forms
{
    public partial class DebugForm : Form
    {
        StringBuilder bufferGeneral = new StringBuilder();
        StringBuilder bufferControl = new StringBuilder();
        StringBuilder bufferGraphics = new StringBuilder();
        StringBuilder bufferSound = new StringBuilder();

        public DebugForm()
        {
            InitializeComponent();
            EventHandler textChanged = new EventHandler(delegate(object sender, EventArgs e)
            {
                TextBox textbox = (TextBox)sender;
                string[] lines = textbox.Lines;
                if (lines.Length >= 2000)
                {
                    const int MAX_LINE = 500;
                    StringBuilder sb = new StringBuilder();
                    for (int i = lines.Length - MAX_LINE; i < lines.Length; i++)
                    {
                        sb.AppendLine(lines[i]);
                    }
                    textbox.Clear();
                    textbox.AppendText(sb.ToString());
                }
            });

            Timer timer = new Timer();
            timer.Interval = 20;
            timer.Tick += delegate(object sender, EventArgs e)
            {
                lock(this)
                {
                    Action<TextBox, StringBuilder> func = delegate(TextBox textbox, StringBuilder sb)
                    {
                        if (sb.Length > 0)
                        {
                            int length1 = textbox.Text.Length;
                            int length2 = sb.Length;
                            string s = sb.ToString();
                            textbox.AppendText(s);
                            sb.Clear();
                        }
                    };
                    func(textBoxGeneral, bufferGeneral);
                    func(textBoxControl, bufferControl);
                    func(textBoxGraphics, bufferGraphics);
                    func(textBoxSound, bufferSound);
                }
            };

            textBoxGeneral.MaxLength = int.MaxValue;
            textBoxControl.MaxLength = int.MaxValue;
            textBoxGraphics.MaxLength = int.MaxValue;
            textBoxSound.MaxLength = int.MaxValue;

            textBoxGeneral.TextChanged += textChanged;
            textBoxControl.TextChanged += textChanged;
            textBoxGraphics.TextChanged += textChanged;
            textBoxSound.TextChanged += textChanged;

            timer.Start();
        }

        private static void AppendMessage(Object lockObj, CheckBox checkbox, StringBuilder sb, string s)
        {
            lock (lockObj)
            {
                if (checkbox.Checked)
                {
                    sb.AppendLine(string.Format("[{0}] {1}", DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss.fff"), s));
                }
            }
        }
 
        public void AppendGeneralMessage(string s)
        {
            AppendMessage(this, checkBoxGeneral, bufferGeneral, s);
        }
        
        public void AppendControlMessage(string s)
        {
            AppendMessage(this, checkBoxControl, bufferControl, s);
        }
        
        public void AppendGraphicsMessage(string s)
        {
            AppendMessage(this, checkBoxGraphics, bufferGraphics, s);
        }
        
        public void AppendSoundMessage(string s)
        {
            AppendMessage(this, checkBoxSound, bufferSound, s);
        }

        private void button_Click(object sender, EventArgs e)
        {
            bool flg = true;
            flg &= checkBoxGeneral.Checked;
            flg &= checkBoxControl.Checked;
            flg &= checkBoxGraphics.Checked;
            flg &= checkBoxSound.Checked;
            checkBoxGeneral.Checked = !flg;
            checkBoxControl.Checked = !flg;
            checkBoxGraphics.Checked = !flg;
            checkBoxSound.Checked = !flg;
        }
    }
}
