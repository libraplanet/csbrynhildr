﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CsBrynhildr.Commons
{
    public class ControlInputsRecorder
    {
        const int VK_SHIFT = 0x10;
        const int VK_CONTROL = 0x11;
        const int VK_MENU = 0x12;
        const int VK_HANJA = 0x19;
        const int VK_OEM_AUTO = 0xF3;
        const int VK_OEM_ENLW = 0xF4;
        
        public class Inputs
        {
            public bool IsMouseMove { get; set; }
            public int MouseX { get; set; }
            public int MouseY { get; set; }
            public int MouseLeft { get; set; }
            public int MouseRight { get; set; }
            public int MouseWheel { get; set; }
            public bool IsKeyDown { get; set; }
            public int keyLogIndex = 0;
            public sbyte[] keyCode = new sbyte[10];
            public sbyte[] keyFlg = new sbyte[10];

            public sbyte KeyCode
            {
                get
                {
                    return keyCode[0];
                }
            }

            public sbyte KeyFlg
            {
                get
                {
                    return keyFlg[0];
                }
            }

            public Inputs()
            {
                Init();
            }
            void Init()
            {
                IsMouseMove = false;
                MouseX = 0;
                MouseY = 0;
                MouseLeft = 0;
                MouseRight = 0;
                MouseWheel = 0;
                IsKeyDown = false;
                keyLogIndex = 0;
                keyCode[0] = 0;
                keyFlg[0] = 0;
            }
        }

        Inputs[] inputs = new Inputs[10];

        public Func<bool> IsFocus;

        public ControlInputsRecorder()
        {
            for (int i = 0; i < inputs.Length; i++)
            {
                inputs[i] = new Inputs();
            }
        }

        public Inputs GetCurrentInput(bool isSlide)
        {
            lock(this)
            {
                Inputs ret = inputs[0];
                if (isSlide)
                {
                    Array.Copy(inputs, 0, inputs, 1, inputs.Length - 1);
                    inputs[0] = new Inputs();
                    inputs[0].MouseX = inputs[1].MouseX;
                    inputs[0].MouseY = inputs[1].MouseY;
                }
                return ret;
            }
        }

        private void MouseButton(MouseButtons button, int code)
        {
            lock(this)
            {
                if (button == MouseButtons.Left)
                {
                    inputs[0].MouseLeft = code;
                    inputs[0].IsMouseMove = true;
                }
                else if (button == MouseButtons.Right)
                {
                    inputs[0].MouseRight = code;
                    inputs[0].IsMouseMove = true;
                }
            }
        }

        public void EventMouseHover(object sender, EventArgs e)
        {
            lock (this)
            {
            }
        }

        public void EventMouseLeave(object sender, EventArgs e)
        {
            lock (this)
            {
            }
        }

        public void EventMouseMove(object sender, MouseEventArgs e)
        {
            lock (this)
            {
                inputs[0].MouseX = e.X;
                inputs[0].MouseY = e.Y;
                inputs[0].IsMouseMove = true;
            }
        }

        public void EventMouseDown(object sender, MouseEventArgs e)
        {
            lock (this)
            {
                MouseButton(e.Button, 1);
            }
        }

        public void EventMouseUp(object sender, MouseEventArgs e)
        {
            lock (this)
            {
                MouseButton(e.Button, 2);
            }
        }

        public void EventMouseDoubleClick(object sender, MouseEventArgs e)
        {
            lock (this)
            {
                MouseButton(e.Button, 3);
            }
        }

        public void EventMouseWheel(object sender, MouseEventArgs e)
        {
            lock (this)
            {
                const int MAX = 9 * 120;
                int delta = e.Delta;
                if (delta != 0)
                {
                    int sign = Math.Sign(delta);
                    int aVal = Math.Min(Math.Abs(delta), MAX);
                    int val = sign * aVal / 10;
                    inputs[0].MouseWheel = val;
                }
            }
        }

        public void EventKeyUp(object sender, KeyEventArgs e)
        {
            lock (this)
            {
                if((IsFocus != null) || (IsFocus()))
                {
                    int index = inputs[0].keyLogIndex;
                    if (index < inputs[0].keyFlg.Length)
                    {
                        inputs[0].keyFlg[index] = (sbyte)0x00;
                        inputs[0].keyCode[index] = (sbyte)e.KeyCode;
                        inputs[0].IsKeyDown = true;
                        inputs[0].keyLogIndex++;
                    }
                }
            }
        }

        public void EventKeyDown(object sender, KeyEventArgs e)
        {
            lock (this)
            {
                if ((IsFocus != null) || (IsFocus()))
                {
                    bool flg = true;
                    if(flg)
                    {
                        int index = inputs[0].keyLogIndex;
                        if (index < inputs[0].keyFlg.Length)
                        {
                            inputs[0].keyFlg[index] = unchecked((sbyte)0x80);
                            inputs[0].keyCode[index] = (sbyte)e.KeyCode;
                            inputs[0].IsKeyDown = true;
                            inputs[0].keyLogIndex++;
                        }
                    }
                }
            }
        }
    }
}
