﻿//#define DOUBLE_BUFFER
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

using SdlWrapper;

namespace CsBrynhildr.Controls
{
    public partial class SdlControl : UserControl
    {
        public IntPtr SdlWindow { get; private set; }
        public IntPtr SdlSurface { get; private set; }

        public SdlControl()
        {
            InitializeComponent();
            this.SuspendLayout();
            try
            {
                SdlWindow = Sdl.SDL_CreateWindowFrom(this.Handle);
#if DOUBLE_BUFFER
                SdlSurface = IntPtr.Zero;
                CreateBuckBuffer();
#else
                SdlSurface = Sdl.SDL_GetWindowSurface(SdlWindow);
#endif
            }
            catch { }
            finally
            {
                this.ResumeLayout(false);
            }
        }

#if DOUBLE_BUFFER
        private void CreateBuckBuffer()
        {
            int w = this.Width;
            int h = this.Height;
            IntPtr newSurface = Sdl.SDL_CreateRGBSurface(Sdl.SDL_SWSURFACE, w, h, 32, Sdl.RMASK, Sdl.GMASK, Sdl.BMASK, Sdl.AMASK);

            if (SdlSurface != IntPtr.Zero)
            {
                Sdl.SDL_UpperBlit(SdlSurface, IntPtr.Zero, newSurface, IntPtr.Zero);
                Sdl.SDL_FreeSurface(SdlSurface);
            }

            this.SdlSurface = newSurface;
        }
#endif

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
#if DOUBLE_BUFFER
            try
            {
                CreateBuckBuffer();
            }
            catch { }
#else
            SdlSurface = Sdl.SDL_GetWindowSurface(SdlWindow);
#endif
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            UpdateSdlControl();
        }

        public void UpdateSdlControl()
        {
            if (SdlWindow != IntPtr.Zero)
            {
#if DOUBLE_BUFFER
                IntPtr surface = Sdl.SDL_GetWindowSurface(SdlWindow);
                if (surface != IntPtr.Zero)
                {
                    Sdl.SDL_UpperBlit(SdlSurface, IntPtr.Zero, surface, IntPtr.Zero);
                    Sdl.SDL_UpdateWindowSurface(SdlWindow);
                }
#else
                Sdl.SDL_UpdateWindowSurface(SdlWindow);
#endif
            }
        }
    }
}
