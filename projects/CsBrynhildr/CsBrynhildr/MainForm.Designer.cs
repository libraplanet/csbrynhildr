﻿namespace CsBrynhildr
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenuSystem = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemFile = new System.Windows.Forms.MenuItem();
            this.menuItemConnect = new System.Windows.Forms.MenuItem();
            this.menuItemOpen = new System.Windows.Forms.MenuItem();
            this.menuItemSep0 = new System.Windows.Forms.MenuItem();
            this.menuItemTest = new System.Windows.Forms.MenuItem();
            this.menuItemDisconnect = new System.Windows.Forms.MenuItem();
            this.menuItemSep2 = new System.Windows.Forms.MenuItem();
            this.menuItemExit = new System.Windows.Forms.MenuItem();
            this.menuItemWindow = new System.Windows.Forms.MenuItem();
            this.menuItemTopMost = new System.Windows.Forms.MenuItem();
            this.menuItemFullScreen = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScaleSelect = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale025 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale050 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale075 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale100 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale125 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale150 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale175 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale200 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScale300 = new System.Windows.Forms.MenuItem();
            this.menuItemRenderScaleFit = new System.Windows.Forms.MenuItem();
            this.menuItemControl = new System.Windows.Forms.MenuItem();
            this.menuItemInput = new System.Windows.Forms.MenuItem();
            this.menuItemGraphics = new System.Windows.Forms.MenuItem();
            this.menuItemFpsSelect = new System.Windows.Forms.MenuItem();
            this.menuItemFps010 = new System.Windows.Forms.MenuItem();
            this.menuItemFps020 = new System.Windows.Forms.MenuItem();
            this.menuItemFps030 = new System.Windows.Forms.MenuItem();
            this.menuItemFps040 = new System.Windows.Forms.MenuItem();
            this.menuItemFps050 = new System.Windows.Forms.MenuItem();
            this.menuItemFps060 = new System.Windows.Forms.MenuItem();
            this.menuItemFps070 = new System.Windows.Forms.MenuItem();
            this.menuItemFps080 = new System.Windows.Forms.MenuItem();
            this.menuItemFps090 = new System.Windows.Forms.MenuItem();
            this.menuItemFps100 = new System.Windows.Forms.MenuItem();
            this.menuItemFpsMax = new System.Windows.Forms.MenuItem();
            this.menuItemQualitySelect = new System.Windows.Forms.MenuItem();
            this.menuItemQualityLowest = new System.Windows.Forms.MenuItem();
            this.menuItemQualityLow = new System.Windows.Forms.MenuItem();
            this.menuItemQualityNormal = new System.Windows.Forms.MenuItem();
            this.menuItemQualityHight = new System.Windows.Forms.MenuItem();
            this.menuItemQualityHighest = new System.Windows.Forms.MenuItem();
            this.menuItemZoomSelect = new System.Windows.Forms.MenuItem();
            this.menuItemZoom025 = new System.Windows.Forms.MenuItem();
            this.menuItemZoom050 = new System.Windows.Forms.MenuItem();
            this.menuItemZoom075 = new System.Windows.Forms.MenuItem();
            this.menuItemZoom100 = new System.Windows.Forms.MenuItem();
            this.menuItemHelp = new System.Windows.Forms.MenuItem();
            this.menuItemVersion = new System.Windows.Forms.MenuItem();
            this.menuItemDebugWindow = new System.Windows.Forms.MenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelConnnect = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelTraffic = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelRoot = new System.Windows.Forms.Panel();
            this.sdlControl = new CsBrynhildr.Controls.SdlControl();
            this.statusStrip.SuspendLayout();
            this.panelRoot.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainMenuSystem
            // 
            this.mainMenuSystem.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFile,
            this.menuItemWindow,
            this.menuItemControl,
            this.menuItemGraphics,
            this.menuItemHelp});
            // 
            // menuItemFile
            // 
            this.menuItemFile.Index = 0;
            this.menuItemFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemConnect,
            this.menuItemOpen,
            this.menuItemSep0,
            this.menuItemTest,
            this.menuItemDisconnect,
            this.menuItemSep2,
            this.menuItemExit});
            this.menuItemFile.Text = "&File";
            // 
            // menuItemConnect
            // 
            this.menuItemConnect.Index = 0;
            this.menuItemConnect.Text = "&Connect...";
            this.menuItemConnect.Click += new System.EventHandler(this.menuItemConnect_Click);
            // 
            // menuItemOpen
            // 
            this.menuItemOpen.Index = 1;
            this.menuItemOpen.Text = "&Open...";
            this.menuItemOpen.Click += new System.EventHandler(this.menuItemOpen_Click);
            // 
            // menuItemSep0
            // 
            this.menuItemSep0.Index = 2;
            this.menuItemSep0.Text = "-";
            // 
            // menuItemTest
            // 
            this.menuItemTest.Index = 3;
            this.menuItemTest.Text = "&Test";
            this.menuItemTest.Visible = false;
            this.menuItemTest.Click += new System.EventHandler(this.menuItemTest_Click);
            // 
            // menuItemDisconnect
            // 
            this.menuItemDisconnect.Index = 4;
            this.menuItemDisconnect.Text = "&Disconnect";
            this.menuItemDisconnect.Click += new System.EventHandler(this.menuItemDisconnect_Click);
            // 
            // menuItemSep2
            // 
            this.menuItemSep2.Index = 5;
            this.menuItemSep2.Text = "-";
            // 
            // menuItemExit
            // 
            this.menuItemExit.Index = 6;
            this.menuItemExit.Text = "&Exit";
            this.menuItemExit.Click += new System.EventHandler(this.menuItemExit_Click);
            // 
            // menuItemWindow
            // 
            this.menuItemWindow.Index = 1;
            this.menuItemWindow.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemTopMost,
            this.menuItemFullScreen,
            this.menuItemRenderScaleSelect});
            this.menuItemWindow.Text = "&Window";
            // 
            // menuItemTopMost
            // 
            this.menuItemTopMost.Index = 0;
            this.menuItemTopMost.Text = "&Top Most";
            this.menuItemTopMost.Click += new System.EventHandler(this.menuItemTopMost_Click);
            // 
            // menuItemFullScreen
            // 
            this.menuItemFullScreen.Index = 1;
            this.menuItemFullScreen.Text = "&Full Screen";
            this.menuItemFullScreen.Click += new System.EventHandler(this.menuItemFullScreen_Click);
            // 
            // menuItemRenderScaleSelect
            // 
            this.menuItemRenderScaleSelect.Index = 2;
            this.menuItemRenderScaleSelect.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemRenderScale025,
            this.menuItemRenderScale050,
            this.menuItemRenderScale075,
            this.menuItemRenderScale100,
            this.menuItemRenderScale125,
            this.menuItemRenderScale150,
            this.menuItemRenderScale175,
            this.menuItemRenderScale200,
            this.menuItemRenderScale300,
            this.menuItemRenderScaleFit});
            this.menuItemRenderScaleSelect.Text = "&Render Scale";
            // 
            // menuItemRenderScale025
            // 
            this.menuItemRenderScale025.Index = 0;
            this.menuItemRenderScale025.Text = "(&1) 25%";
            this.menuItemRenderScale025.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale050
            // 
            this.menuItemRenderScale050.Index = 1;
            this.menuItemRenderScale050.Text = "(&2) 50%";
            this.menuItemRenderScale050.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale075
            // 
            this.menuItemRenderScale075.Index = 2;
            this.menuItemRenderScale075.Text = "(&3) 75%";
            this.menuItemRenderScale075.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale100
            // 
            this.menuItemRenderScale100.Index = 3;
            this.menuItemRenderScale100.Text = "(&4) 100%";
            this.menuItemRenderScale100.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale125
            // 
            this.menuItemRenderScale125.Index = 4;
            this.menuItemRenderScale125.Text = "(&5) 125%";
            this.menuItemRenderScale125.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale150
            // 
            this.menuItemRenderScale150.Index = 5;
            this.menuItemRenderScale150.Text = "(&6) 150%";
            this.menuItemRenderScale150.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale175
            // 
            this.menuItemRenderScale175.Index = 6;
            this.menuItemRenderScale175.Text = "(&7) 175%";
            this.menuItemRenderScale175.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale200
            // 
            this.menuItemRenderScale200.Index = 7;
            this.menuItemRenderScale200.Text = "(&8) 200%";
            this.menuItemRenderScale200.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScale300
            // 
            this.menuItemRenderScale300.Index = 8;
            this.menuItemRenderScale300.Text = "(&9) 300%";
            this.menuItemRenderScale300.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemRenderScaleFit
            // 
            this.menuItemRenderScaleFit.Index = 9;
            this.menuItemRenderScaleFit.Text = "(&0) FIT";
            this.menuItemRenderScaleFit.Click += new System.EventHandler(this.menuItemRenderScale_Click);
            // 
            // menuItemControl
            // 
            this.menuItemControl.Index = 2;
            this.menuItemControl.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemInput});
            this.menuItemControl.Text = "&Control";
            // 
            // menuItemInput
            // 
            this.menuItemInput.Index = 0;
            this.menuItemInput.Text = "&Input";
            this.menuItemInput.Click += new System.EventHandler(this.menuItemInput_Click);
            // 
            // menuItemGraphics
            // 
            this.menuItemGraphics.Index = 3;
            this.menuItemGraphics.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFpsSelect,
            this.menuItemQualitySelect,
            this.menuItemZoomSelect});
            this.menuItemGraphics.Text = "&Graphics";
            // 
            // menuItemFpsSelect
            // 
            this.menuItemFpsSelect.Index = 0;
            this.menuItemFpsSelect.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemFps010,
            this.menuItemFps020,
            this.menuItemFps030,
            this.menuItemFps040,
            this.menuItemFps050,
            this.menuItemFps060,
            this.menuItemFps070,
            this.menuItemFps080,
            this.menuItemFps090,
            this.menuItemFps100,
            this.menuItemFpsMax});
            this.menuItemFpsSelect.Text = "&Fps";
            // 
            // menuItemFps010
            // 
            this.menuItemFps010.Index = 0;
            this.menuItemFps010.Text = "&10";
            this.menuItemFps010.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps020
            // 
            this.menuItemFps020.Index = 1;
            this.menuItemFps020.Text = "&20";
            this.menuItemFps020.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps030
            // 
            this.menuItemFps030.Index = 2;
            this.menuItemFps030.Text = "&30";
            this.menuItemFps030.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps040
            // 
            this.menuItemFps040.Index = 3;
            this.menuItemFps040.Text = "&40";
            this.menuItemFps040.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps050
            // 
            this.menuItemFps050.Index = 4;
            this.menuItemFps050.Text = "&50";
            this.menuItemFps050.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps060
            // 
            this.menuItemFps060.Index = 5;
            this.menuItemFps060.Text = "&60";
            this.menuItemFps060.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps070
            // 
            this.menuItemFps070.Index = 6;
            this.menuItemFps070.Text = "&70";
            this.menuItemFps070.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps080
            // 
            this.menuItemFps080.Index = 7;
            this.menuItemFps080.Text = "&80";
            this.menuItemFps080.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps090
            // 
            this.menuItemFps090.Index = 8;
            this.menuItemFps090.Text = "&90";
            this.menuItemFps090.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFps100
            // 
            this.menuItemFps100.Index = 9;
            this.menuItemFps100.Text = "&100";
            this.menuItemFps100.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemFpsMax
            // 
            this.menuItemFpsMax.Index = 10;
            this.menuItemFpsMax.Text = "&MAX";
            this.menuItemFpsMax.Click += new System.EventHandler(this.menuItemFps_Click);
            // 
            // menuItemQualitySelect
            // 
            this.menuItemQualitySelect.Index = 1;
            this.menuItemQualitySelect.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemQualityLowest,
            this.menuItemQualityLow,
            this.menuItemQualityNormal,
            this.menuItemQualityHight,
            this.menuItemQualityHighest});
            this.menuItemQualitySelect.Text = "&Quality";
            // 
            // menuItemQualityLowest
            // 
            this.menuItemQualityLowest.Index = 0;
            this.menuItemQualityLowest.Text = "(&1) Lowest";
            this.menuItemQualityLowest.Click += new System.EventHandler(this.menuItemQuality_Click);
            // 
            // menuItemQualityLow
            // 
            this.menuItemQualityLow.Index = 1;
            this.menuItemQualityLow.Text = "(&3) Low";
            this.menuItemQualityLow.Click += new System.EventHandler(this.menuItemQuality_Click);
            // 
            // menuItemQualityNormal
            // 
            this.menuItemQualityNormal.Index = 2;
            this.menuItemQualityNormal.Text = "(&5) Normal";
            this.menuItemQualityNormal.Click += new System.EventHandler(this.menuItemQuality_Click);
            // 
            // menuItemQualityHight
            // 
            this.menuItemQualityHight.Index = 3;
            this.menuItemQualityHight.Text = "(&7) Hight";
            this.menuItemQualityHight.Click += new System.EventHandler(this.menuItemQuality_Click);
            // 
            // menuItemQualityHighest
            // 
            this.menuItemQualityHighest.Index = 4;
            this.menuItemQualityHighest.Text = "(&9) Highest";
            this.menuItemQualityHighest.Click += new System.EventHandler(this.menuItemQuality_Click);
            // 
            // menuItemZoomSelect
            // 
            this.menuItemZoomSelect.Index = 2;
            this.menuItemZoomSelect.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemZoom025,
            this.menuItemZoom050,
            this.menuItemZoom075,
            this.menuItemZoom100});
            this.menuItemZoomSelect.Text = "&Zoom";
            // 
            // menuItemZoom025
            // 
            this.menuItemZoom025.Index = 0;
            this.menuItemZoom025.Text = "(&1) 25%";
            this.menuItemZoom025.Click += new System.EventHandler(this.menuItemZoom_Click);
            // 
            // menuItemZoom050
            // 
            this.menuItemZoom050.Index = 1;
            this.menuItemZoom050.Text = "(&2) 50%";
            this.menuItemZoom050.Click += new System.EventHandler(this.menuItemZoom_Click);
            // 
            // menuItemZoom075
            // 
            this.menuItemZoom075.Index = 2;
            this.menuItemZoom075.Text = "(&3) 75%";
            this.menuItemZoom075.Click += new System.EventHandler(this.menuItemZoom_Click);
            // 
            // menuItemZoom100
            // 
            this.menuItemZoom100.Index = 3;
            this.menuItemZoom100.Text = "(&4) 100%";
            this.menuItemZoom100.Click += new System.EventHandler(this.menuItemZoom_Click);
            // 
            // menuItemHelp
            // 
            this.menuItemHelp.Index = 4;
            this.menuItemHelp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemVersion,
            this.menuItemDebugWindow});
            this.menuItemHelp.Text = "&Help";
            // 
            // menuItemVersion
            // 
            this.menuItemVersion.Index = 0;
            this.menuItemVersion.Text = "&Version...";
            this.menuItemVersion.Click += new System.EventHandler(this.menuItemVersion_Click);
            // 
            // menuItemDebugWindow
            // 
            this.menuItemDebugWindow.Index = 1;
            this.menuItemDebugWindow.Text = "&Debug Window...";
            this.menuItemDebugWindow.Click += new System.EventHandler(this.menuItemDebugWindow_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelConnnect,
            this.toolStripStatusLabelTraffic});
            this.statusStrip.Location = new System.Drawing.Point(0, 328);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(589, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripStatusLabelConnnect
            // 
            this.toolStripStatusLabelConnnect.Name = "toolStripStatusLabelConnnect";
            this.toolStripStatusLabelConnnect.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabelTraffic
            // 
            this.toolStripStatusLabelTraffic.Name = "toolStripStatusLabelTraffic";
            this.toolStripStatusLabelTraffic.Size = new System.Drawing.Size(0, 17);
            // 
            // panelRoot
            // 
            this.panelRoot.AutoScroll = true;
            this.panelRoot.BackColor = System.Drawing.Color.Black;
            this.panelRoot.Controls.Add(this.sdlControl);
            this.panelRoot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRoot.Location = new System.Drawing.Point(0, 0);
            this.panelRoot.Name = "panelRoot";
            this.panelRoot.Size = new System.Drawing.Size(589, 328);
            this.panelRoot.TabIndex = 1;
            // 
            // sdlControl
            // 
            this.sdlControl.Location = new System.Drawing.Point(0, 0);
            this.sdlControl.Name = "sdlControl";
            this.sdlControl.Size = new System.Drawing.Size(0, 0);
            this.sdlControl.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 350);
            this.Controls.Add(this.panelRoot);
            this.Controls.Add(this.statusStrip);
            this.Menu = this.mainMenuSystem;
            this.Name = "MainForm";
            this.Text = "CsBrynhildr";
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.panelRoot.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MainMenu mainMenuSystem;
        private System.Windows.Forms.MenuItem menuItemFile;
        private System.Windows.Forms.MenuItem menuItemExit;
        private System.Windows.Forms.MenuItem menuItemHelp;
        private System.Windows.Forms.MenuItem menuItemVersion;
        private System.Windows.Forms.MenuItem menuItemWindow;
        private System.Windows.Forms.MenuItem menuItemTopMost;
        private System.Windows.Forms.MenuItem menuItemFullScreen;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelTraffic;
        private System.Windows.Forms.Panel panelRoot;
        private Controls.SdlControl sdlControl;
        private System.Windows.Forms.MenuItem menuItemDebugWindow;
        private System.Windows.Forms.MenuItem menuItemFpsSelect;
        private System.Windows.Forms.MenuItem menuItemFps010;
        private System.Windows.Forms.MenuItem menuItemFps020;
        private System.Windows.Forms.MenuItem menuItemFps030;
        private System.Windows.Forms.MenuItem menuItemFps040;
        private System.Windows.Forms.MenuItem menuItemFps050;
        private System.Windows.Forms.MenuItem menuItemFps060;
        private System.Windows.Forms.MenuItem menuItemFps070;
        private System.Windows.Forms.MenuItem menuItemFps080;
        private System.Windows.Forms.MenuItem menuItemFps090;
        private System.Windows.Forms.MenuItem menuItemFps100;
        private System.Windows.Forms.MenuItem menuItemGraphics;
        private System.Windows.Forms.MenuItem menuItemFpsMax;
        private System.Windows.Forms.MenuItem menuItemRenderScaleSelect;
        private System.Windows.Forms.MenuItem menuItemRenderScale025;
        private System.Windows.Forms.MenuItem menuItemRenderScale050;
        private System.Windows.Forms.MenuItem menuItemRenderScale075;
        private System.Windows.Forms.MenuItem menuItemRenderScale100;
        private System.Windows.Forms.MenuItem menuItemRenderScale125;
        private System.Windows.Forms.MenuItem menuItemRenderScale150;
        private System.Windows.Forms.MenuItem menuItemRenderScale175;
        private System.Windows.Forms.MenuItem menuItemRenderScale200;
        private System.Windows.Forms.MenuItem menuItemRenderScale300;
        private System.Windows.Forms.MenuItem menuItemQualitySelect;
        private System.Windows.Forms.MenuItem menuItemQualityLowest;
        private System.Windows.Forms.MenuItem menuItemQualityLow;
        private System.Windows.Forms.MenuItem menuItemQualityNormal;
        private System.Windows.Forms.MenuItem menuItemQualityHight;
        private System.Windows.Forms.MenuItem menuItemQualityHighest;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelConnnect;
        private System.Windows.Forms.MenuItem menuItemZoomSelect;
        private System.Windows.Forms.MenuItem menuItemZoom025;
        private System.Windows.Forms.MenuItem menuItemZoom050;
        private System.Windows.Forms.MenuItem menuItemZoom075;
        private System.Windows.Forms.MenuItem menuItemZoom100;
        private System.Windows.Forms.MenuItem menuItemConnect;
        private System.Windows.Forms.MenuItem menuItemOpen;
        private System.Windows.Forms.MenuItem menuItemSep2;
        private System.Windows.Forms.MenuItem menuItemControl;
        private System.Windows.Forms.MenuItem menuItemInput;
        private System.Windows.Forms.MenuItem menuItemSep0;
        private System.Windows.Forms.MenuItem menuItemDisconnect;
        private System.Windows.Forms.MenuItem menuItemTest;
        private System.Windows.Forms.MenuItem menuItemRenderScaleFit;

    }
}